#!/usr/bin/perl -w

# $HeadURL: https://wush.net/svn/ctools/ctstats/drraw/trunk/fwd.cgi $
# $Id: fwd.cgi 1525 2012-03-01 20:34:49Z dlhaines $

use strict;
use CGI;
use URI;

my $FORM = new CGI;
my($nextUrl) = $FORM->param('url') || new URI($FORM->referer) || "/stats-bin/drraw.cgi";
my $username = $ENV{'REMOTE_USER'};

if ($username ne ""){
	#exit print "Location: ${nextUrl}\n\n";
	print $FORM->header(-type=>'text/html');
	exit print <<REDIR;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<META http-equiv="refresh" content="0; URL=${nextUrl}">
</HEAD>
</HTML>
REDIR
} else {
	print $FORM->header(-type=>'text/html', -status=> "500");
	print $FORM->start_html(-title=>'login unsuccessful');
}
