#!/usr/local/bin/perl
#!/sw/bin/perl
##
##  printenv -- demo CGI program which just prints its environment
##
## $HeadURL: https://wush.net/svn/ctools/ctstats/drraw/trunk/env.cgi $
## $Id: env.cgi 1525 2012-03-01 20:34:49Z dlhaines $

print "Content-type: text/plain\n\n";

print "Perl Version: ",$],"\n";

foreach $var (sort(keys(%ENV))) {
    $val = $ENV{$var};
    $val =~ s|\n|\\n|g;
    $val =~ s|"|\\"|g;
    print "${var}=\"${val}\"\n";
}
