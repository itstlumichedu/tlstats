#
# drraw configuration template/sample
#
# $HeadURL: https://wush.net/svn/ctools/ctstats/drraw/trunk/drraw.conf $
# $Id: drraw.conf 2220 2015-12-16 14:41:16Z pushyami $
#
# This file should be installed in the same directory as drraw.cgi and
# edited as needed to customize your installation.  At a minimum, you
# need to edit variables which are not commented (below) since
#

### note that 7/1/2012 at midnight is 1341115200 in epoch time

# this will help catch configuration mistakes
use strict;

# The title used as header for the index web pages
if($ENV{'SERVER_NAME'} eq "tlstats.dsc.umich.edu"){
$title = 'TLStats - Journey';
}else{
$title = 'TLStatsDEV - Fiesta';
}
# If you want to add a custom header for all pages, define this..
#
# UM Ctstats local mod -- use a different header for logged in vs logged out users (Cosign)
# The script tags are added to the right below the <body> tag when loads. Angular library addition is needed
$header = '<script type="text/javascript" src="/vendors/js/jquery.min.js"></script> <script type="text/javascript" src="/vendors/js/angular.js"></script><script type="text/javascript" src="/js/ctstats.js"></script><table border="0" width="100%"><tr><td align="right" valign="top">';
my($myurl) = $ENV{'SCRIPT_URI'};
my($myreqparam) = $ENV{'QUERY_STRING'};
if ($myreqparam ne ""){
	$myurl .= "?" . $myreqparam;
}
my($logoutNext) = $myurl;
if ($myreqparam =~ /(\&|\;)?USERSAID\=Edit(\&|\;)?/ || $myreqparam =~ /Type\=DB/){
	$logoutNext =~ s/(\&|\;)?USERSAID\=Edit(\&|\;)?/$1/g;
	$logoutNext =~ s/(\&|\;)?Type\=DB(\&|\;)?/Mode\=view$1$2/g;
}
my($eurl) = CGI::escape($myurl);
if ($ENV{'REMOTE_USER'} ne ""){
#<a href="/stats-bin/logout.cgi?${logoutNext}">[Logout ${ENV{'REMOTE_USER'}}]</a> # was original of Logout line below
	$header .=<<HEAD;
<a href="/cgi-bin/logout.cgi?${logoutNext}">[Logout ${ENV{'REMOTE_USER'}}]</a>
HEAD
} else {
#<a href="/stats-bin/fwd.cgi?url=$eurl">[Login]</a> # was original of Login line below
	$header .=<<HEAD;
<a href="/cgi-bin/fwd.cgi?url=$eurl">[Login]</a>
HEAD
}
$header .= "</td></tr></table>";

# If you want to add a custom footer for all pages, define this..
$footer =<<FOOT;
NB. <img src="/icons/unknown.gif" title="unk" /> means there is no info to draw a graph for the selected time-period
<br/>
questions? ask dlhaines\@umich.edu
($ENV{'SERVER_ADDR'})
FOOT

# Directories where Round Robin Databases may be found
# They will be searched recursively for files matching *.rrd (Round Robin
# Database files) and *.evt (Event files).
%datadirs = (
	     '/usr/local/ctools/app/ctools/data' => 'DATA',
	     '/usr/local/ctools/app/ctools/evt' => 'EVT',
            );

# This function is used to sort *.rrd and *.evt filenames before displaying
# them for the user to choose from.  It is passed the two arguments that
# need to be compared.
#sub mydatafnsort { return $_[0] cmp $_[1]; }

# Default DS filter when adding graph data sources from RRD files
#$dsfilter_def = '';

# Names of defined RRAs and the names to use on the graphs
#@rranames = ( 'MIN', 'AVERAGE', 'MAX', 'LAST' );
#%rranames = ( 'MIN'    => 'Min',
#             'AVERAGE' => 'Avg',
#             'MAX'     => 'Max',
#             'LAST'    => 'Last'
#            );


# Viewer automatic refresh timer (seconds)
#$vrefresh = '900';
# Minimum dashboard automatic refresh timer (seconds)
#$drefresh = 1800;

## Add view to the start of ctir

our $ctir_start = 1341115200;

# Default Views and their names
@dv_def  = ( 'end - 28 hours', 'end - 1 week', 'end - 1 month', 'end - 3 months', 'end - 1 year', 'end - 2 years', '7/1/2012' );
@dv_name = ( 'Past 28 Hours', 'Past Week', 'Past Month', 'Past Quarter', 'Past Year', 'Past 2 Years', 'CTIR start');
# How many seconds for each of the above views?
# (Ignored when using rrdtool 1.0.47 or later)
## 
@dv_secs = ( 100800, 604800, 2419200, 7257600, 31536000, 63072000, time - $ctir_start);

# Format used for the "Additional GPRINTs"
#$gformat = "%8.2lf";

# Graph Overlay (Requires RRDtool 1.0.36 or later)
#$overlay = '/somewhere/logo.gd';

# Maximum time (seconds) CGI processes may be running (0 to disable)
$maxtime = 60;

# Cache refresh time (seconds)
#$crefresh = 900;
$crefresh = 60; ## For debugging, don't cache results for long.

# Where to save data and store temporary files
# These directories MUST EXIST and the user running CGI scripts must have
# read AND write access to them.  It is entirely safe to delete any content
# found in the temporary directory.
$saved_dir = '/usr/local/ctools/app/ctools/var/drraw/saved';
$tmp_dir   = '/usr/local/ctools/app/ctools/var/drraw/cache';

# By default, critical errors are shown in the produced HTML pages produced
# and sent to standard error (which web servers typically write to some
# logfile).  If the following is defined, such errors will be written to
# file directly by drraw.
$ERRLOG = '/usr/local/ctools/app/ctools/var/log/drraw.errors.log';
# By default (as long as the Digest::MD5 Perl module is available), drraw
# will cache images to avoid recomputing them needlessly.  The following
# sets how often drraw should purge expired images from the cache.
# (Expired images being are automatically purged when refreshed.)
# Setting this to 0 will disable caching altogether.
$clean_cache = 21600; # 6 hours
# Whether or not RCS will be used, this requires:
# (1) The Perl "Rcs" module to be installed
# (2) The $saved_dir/RCS directory to exist
## Need to fix RCS usage before turning it on.


#$use_rcs = 0; # Don't use Rcs.
#$use_rcs = 1; # Do use Rcs.
$use_rcs = 2; # Use Rcs and provide error messages if there is a problem.

# set this if need to have a non-default directory for the rcs binaries.
$rcs_bindir = "/usr/bin";

# Security Levels:
#       0 : Read-Only (e.g. graph viewing only)
#       1 : Limited Read-Write (such users can define/save new graphs,
#                               and delete saved graphs they saved)
#       2 : Read-Write
# User authentication/validation handling is left up to the web server.  In
# other words, for things to work, you will need to change the web server
# configuration.  If you don't, nothing will happen :-)
#
# Authenticated users automatically set the level to 2 unless the username
# is found in %users (below) in which case the level is set accordingly to
# what is defined.
# Unauthenticated users get the 'guest' username.
#
# If you don't care for authentication, set the following to 2.
%users = ( 
	'cousinea' => 2,
	'ckretler' => 2,
	'rkcarter' => 2,
	'dlhaines' => 2,
	'ctools' => 2,
	'guest' => 1,
	 );

# By default, drraw will use the REMOTE_USER environment variable to find
# the user's username.  If you wish to use something else, simply define
# the following subroutine.
# NOTES: (1) drraw will fall back to using the REMOTE_USER environment variable
#            if this subroutine returns 'undef': use 'guest' if you wish to
#            leave the user unauthenticated.
#        (2) this subroutine may manipulate the %users array based on external
#            data allowing to process authentication entirely out of this
#            configuration file.
#sub &mygetuser { return $ENV{'SSL_CLIENT_EMAIL'}; }
sub mygetuser { return 'ctools'; }

# If you want to override your system's default timezone, you may do so here.
#$ENV{'TZ'} = 'UCT';

# If you have many RRD files and templates, indexes can become extremely
# big and slow to load.  This variable allows you to prevent all templates
# choices from being displayed in the indexes based on how many choices
# are available per template.
$IndexMax = 100;

# Custom Index Definition
#
# Custom Indexes are defined as a HASH:
#  - Each key is the name that will be presented to the user
#  - Keys are sorted when the index is built
#  - If a key matches ^\d+\s+(\S.*)$ then only $1 will be shown
#    (This allows for keys to be sorted independently of what is shown.)
#  - The value for a given key must be one of the following:
#    - A  HASH   (for nested indexes)
#    - A  SCALAR (for leaves) which will be matched as a regular expression
#      against the Graph titles, Template titles and Dashboard titles.
#      Matching titles will be presented to the user.
#    - An ARRAY  (for leaves) containing 3 elements.  Each element will be
#      matched as a regular expression against the Graph titles, Template
#      titles and Dashboard titles (respectively).  Matching titles will
#      be presented to the user.
#
# For example:
# %Index = ( '1 Network' => { 'Cisco' => [ undef, 'Cisco', 'Cisco' ],
#                             'Interfaces Statistics' => 'Interface',
#                             'Miscellaneous' => [ 'Traffic', '(DMZ Traffic|Firewall)', undef ],
#                             'VPN' => 'VPN' },
#            '2 Storage' => [ undef, '(Disk|SAN)', '(Disk|SAN)' ],
#            '3 UNIX' => [ '(Solaris|BSD)', '(Solaris|BSD)', 'UNIX' ],
#            '4 Windows => [ 'Windows', 'Windows', 'Windows' ],
#            '5 Environmental' => [ 'Datacenter', undef, 'Datacenter' ] );
#
# IMPORTANT NOTE: Nested indexes are implemented using JavaScript.

#      
# Icons used in the browser
# These may be copied from the drraw distribution if they are missing from
# your web server installation.
#$icon_new = '/icons/generic.gif';
#$icon_closed = '/icons/folder.gif';
#$icon_open = '/icons/folder.open.gif';
#$icon_text = '/icons/text.gif';
#$icon_help = '/icons/unknown.gif';
#$icon_bug = '/icons/bomb.gif';
#$icon_link = '/icons/link.gif';

# Custom Style Sheet
# $CSS

1;
