'use strict';
/* global $, angular */


var startTime = function() {
    var h, m, s, t;
    var today = new Date();
    h = today.getHours();
    m = today.getMinutes();
    s = today.getSeconds();
    // add a zero in front of numbers<10
    m = checkTime(m);
    s = checkTime(s);
    $('#txt').text(h + ':' + m + ':' + s);
    t = setTimeout('startTime()', 500);
};

var checkTime = function(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
};

var serverType = function() {

    var server = '';
    if (window.location.href.indexOf('https://tlstats.dsc.umich.edu/') > -1) {
        server = 'prod';
        $('body').css('background', '#81BEF7');
    } else {
        $('body').css('background', '#fff7df');
        server = 'nonprod';
    }
    return server;
};


$(document).ready(function() {
    startTime();
    serverType();
});

// registering the app
var dashIndexApp = angular.module('dashIndexApp', []);

// the apps sole contoller - retrieves url and label variables
//so that the same index page can be used in prod and non-prod contexts
dashIndexApp.controller('DashLangController', function($scope, $http) {
    var server = serverType();
    $('body').addClass(server);

    $http.get('lang/lang.json')
        .then(function(res) {
            $scope.lang = res.data[server];
            $scope.date = new Date();
        });
});