#!/bin/bash
# Run EUE script against specific host.  Start selenium server if necessary.
#
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/runEUEForHost.sh $
# $Id: runEUEForHost.sh 2219 2015-12-11 16:56:04Z pushyami $

#set -x

# Find the directory containing the script (often not the current working directory).
BIN="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# default to this port for the selenium server
PORT=4444

TRACE=0

DEFAULT_SCRIPT=testRunner.rb

## process command line arguments
# get a host name from command line
if [ -z "$1" ]; then
    echo "$0: must specify a host name";
    exit 1;
fi

HOST=$1

# There may be a script name on the command line too.
SCRIPT=$DEFAULT_SCRIPT
if [ ! -z "$2" ]; then
    SCRIPT=$2
fi

if [ $TRACE != 0 ]; then
    echo "HOST: [$HOST] SCRIPT: [$SCRIPT]";
fi

echo "starting: at $(date) for $HOST"

if [ -e $BIN/selenium-server-standalone-2.28.0.jar ]; then
    echo "found sel-server in $BIN"
    SELENIUM_DIR=$BIN
fi

if [ -e $BIN/../lib/selenium-server-standalone-2.28.0.jar ]; then
    echo "found sel-server in $BIN/../lib"
    SELENIUM_DIR=$BIN/../lib
fi

echo "sel_server: [$SELENIUM_DIR]"

# make sure there is a selenium server running.
if  ps -aefww | grep -i $PORT | grep -iv grep >| /dev/null ; then
    : # do nothing (use the null command ':')
else 
    java -jar $SELENIUM_DIR/selenium-server-standalone-2.28.0.jar -port $PORT    >| /dev/null &
    echo "started selenium server";
    sleep 30;
fi

# run script for this host.
TMP=$(mktemp /tmp/EUE.XXXX)
#(ruby -r "$BIN/testRunner.rb" -e "EUERunner.runTest '$HOST'" ) >| $TMP
if [ $TRACE != 0 ]; then
    echo "running ruby -r \"$BIN/$SCRIPT\" -e \"EUERunner.runTest '$HOST'\"";
fi
(ruby -r "$BIN/$SCRIPT" -e "EUERunner.runTest '$HOST'" ) >| $TMP
cat $TMP
rm $TMP
# example command to shut down server if necessary.
#wget http://127.0.0.1:$PORT/selenium-server/driver/?cmd=shutDownSeleniumServer

echo  finished at: $(date)
#end
