#!/usr/bin/perl -w

# collect umiac information from umiac servers

use strict;
local $| = 1;

use FindBin;
use RRDs;

my $homeDir = ${FindBin::Bin}."/..";

require "$homeDir/bin/monitor.pl";

our($config);

$config->{'STEP'} = 60;
$config->{'DEBUG'} = 1;
$config->{'program'} = 'umiacStats';
$config->{'version'} = "0.01";
#print "***** config->homeDir looks wrong ******\n";
#$config->{'homeDir'} = $ENV{'HOME'};
$config->{'homeDir'} = $homeDir;
$config->{'libDir'} = $config->{'homeDir'} . "/lib";
$config->{'mibDir'} = $config->{'libDir'} . "/mibs";


our $sharedDataDir;
$config->{'dataDir'} = $sharedDataDir;

$config->{'timeout'} = 1;

if (! defined $config->{'logfacility'}) {
  $config->{'logfacility'} = 'user';
}

openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

use lib qw($config->{'libDir'});

my(%urls) = (
	     "umiac" => ":8888/getStats",
	    );

sub update {
  my($type) = shift(@_);
  my($RRD);
  my($STEP) = $config->{'STEP'};

  my($hostname) = shift(@_);
  my($api) = shift(@_);
  $RRD = $config->{'homeDir'} . "/data/" . $type . "/" . $hostname . "-" . $api . ".rrd";

  my(@vals) = @_;

  my($t) = time;
  $t = int($t);

  for (my $i=0;$i<=$#vals;$i++) {
    if (! $vals[$i] || $vals[$i] eq '') {
      $vals[$i] = 0;
    }
    my $timeval = $vals[$i];
    chomp($timeval);
    if ($timeval =~ /\d*\:\d+\:\d+/) {
      $timeval =~ s/(\d*)\:(\d+)\:(\d+)//;
      my ($hrs, $mins, $secs) = ($1, $2, $3);
      $vals[$i] = $hrs * 3600 + $mins * 60 + $secs;
    }
  }

  if (! -e $RRD) {
    my($START) = time - (2 * $STEP);
  
    notify('info', "Creating $RRD with step ${STEP} starting at $START");
    ensureRRDDirectory($RRD);
    my($v, $msg) = RRD_create($RRD, $START, $STEP, $type);
    if ($v) {
      notify('err', "couldn't create $RRD because $msg");
      return;
    } 
  } 

  print "updateRRD: [$RRD] t: [$t] vals: [",join("|",@vals),"]";
  print "!!!!!!!!! returning without processing\n";
  return;

  my($rv, $errmsg) = updateRRD($RRD, $t, @vals);
  if ($rv) {
    notify('err', "error updating $RRD : ${errmsg}");
  }
}

sub RRD_create {
  my($RRD, $START, $interval, $type) = @_;
  my(@dses);
  my(@rras) = (
	       "RRA:AVERAGE:0.5:1:43800",
	       "RRA:MAX:0.5:1:43800",
	       "RRA:AVERAGE:0.5:5:3000",
	       "RRA:MAX:0.5:5:3000",
	       "RRA:AVERAGE:0.5:10:5000",
	       "RRA:MAX:0.5:10:5000",
	       "RRA:AVERAGE:0.5:1440:732",
	       "RRA:MAX:0.5:1440:732"
	      );

  # use DERIVE with min of zero to deal with rollovers as per
  # http://oss.oetiker.ch/rrdtool/doc/rrdcreate.en.html
  @dses = (
	   "DS:num:DERIVE:1200:0:U",
	   "DS:tts:DERIVE:1200:0:U",
	  );

  RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses, @rras);

  if (my $error = RRDs::error()) {
    return(1, "Cannot create $RRD: $error");
  } else {
    return(0, "$RRD");
  }
}

my(%farm);
while (<>) {
  next if (/(^$|^#)/);
  chomp;
  my($host, $addr) = split(/\|/);
  $farm{$host} = $addr;
}

for my $host (keys %farm) {
  if ($config->{'DEBUG'}) {
    print STDERR "- starting check of host ${host}\n";
  }
  my($addr) = $farm{$host};
  for my $type (keys %urls) {
    my($url) = "https://" . $addr . $urls{$type};
    if ($type eq "umiac") {
      $url = "http://" . $addr . $urls{$type};
    }
    if ($config->{'DEBUG'}) {
      print STDERR "- getting ${url} with timeout of $config->{'timeout'}\n";
    }
    print "check_httpd: url: [$url]\n";

    my($rval, $code, $status, $s, $tcp, $f, $z, $content, $md5) = check_httpd($url,"","",$config->{'timeout'});

    if ($config->{'DEBUG'}) {
      print STDERR <<CONTENT;
	    -- CONTENT --
	    $url -> $rval|$code|$status|\n$content
	    -- END CONTENT --
CONTENT
    }

    if (defined($rval) && $rval > 0 && $code == 200) {
      $content =~ s/\<body\>/\n/g;
      $content =~ s/\<br\>/|\n/g;
      my(@list) = split(/\n/, $content);
      my($i) = 1;
      my($collectionTime) = int($f);

      for my $line (@list) {
	chomp($line);
	next if ($line =~ /this\ script\ runtime/);
	next if ($line =~ /Unknown\ API\ reference:\ getStats/);
	next unless ($line =~ /(\w+)\|(\d+)\|(\d.*)$/);
	my($tts_in_us) = int($3 * 1000000);
	update("umiac", $host, $1, $2, $tts_in_us);
      }
    } else {
      notify('err', "got ${code} getting ${type} stats from ${host}");
    }
  }
}
