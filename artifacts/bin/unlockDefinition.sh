#!/bin/bash
# unlock the definition for drraw.
#
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/unlockDefinition.sh $
# $Id: unlockDefinition.sh 1741 2012-06-12 22:55:25Z dlhaines $
#
# This will free up a drraw graph. The page stating the failure will give name
# of the definition.

# 1) go to directory containing graph definitions 
#    (/usr/local/ctools/app/ctools/www/cgi-bin/saved) as ctools user.
# 2) break the rcs log (rcs -u <name>)
# 3) make the definition file NOT writeable (chmod a-w <name>)  The RCS checkout
#    will make it writeable.

set -x
set -e

######
DEF_DIR="/usr/local/ctools/app/ctools/www/cgi-bin/saved";

FILE_NAME=$1

set -u

FILE="$DEF_DIR/$FILE_NAME"

if [ "X" == "X$FILE_NAME" ]; then
    echo "must specify a file";
    exit 1;
fi;


if [ -d $FILE ]; then
    echo "must specify a file not a directory: [$FILE]";
    exit 1;
fi;

if [ ! -e $FILE ]; then
    echo "no such file: [$FILE]";
    exit 1;
fi;

echo "remove any rcs lock"
rcs -u $FILE 

echo "make it not writeable"
chmod a-w $FILE

#end

