#

sub printfun {
    #shift(@_);
    my(@vals) = @_;
    for (my $i=0;$i <= $#vals; $i++){
        if (! $vals[$i] || $vals[$i] eq ''){
            $vals[$i] = 0;
        }
    }
    #next if (! @vals);
    #next if (! $vals[0] || ! $vals[1]);
    print join('|', @vals) . "|\n";
}



sub getStatTable {
    my($user, $host, $id, $statcmd, $callback) = @_;

    #ssh ro@boxcar "stats show -O print_zero_values=off -c -d | volume:*:read_data volume:*:read_latency volume:*:read_ops volume:*:write_data volume:*:write_latency volume:*:write_ops volume:*:other_ops volume:*:other_latency"

    my(@ret) = `ssh ${user}\@${host} "stats stop -I ${id} -O print_zero_values=off -c -d |" 2>&1`;
    notify('debug', "stats stop ${id}\@${host} returned $#ret rows");
# http://now.netapp.com/NOW/knowledge/docs/ontap/rel7311/html/ontap/sysadmin/monitoring/task/t_oc_mntr_sysinfo-stats-command-background-mode.html
    my(@stopconfirm) = `ssh ${user}\@${host} "stats show ${id}" 2>&1`;
    my($stopconfirmcmd) = join(' ', @stopconfirm);
    notify('debug', "stats show ${id}\@${host} returned $#stopconfirm rows: ${stopconfirmcmd}");
    if ($#stopconfirm > 0 && $stopconfirmcmd !~ /Unknown\/invalid object name \'/){
	notify('crit', "stopping all running stats jobs on ${user}\@${host} ${id}, so will not start again");
	notify('crit', "${id}\@${host} show returned: ${stopconfirmcmd}");
	my(@finalstop) = `ssh ${user}\@${host} "stats stop -a" 2>&1`;
	my($retmsg) = "final stop issued ${user}\@${host} and returned: " . join(' ', @finalstop);
	notify('warning', $retmsg);
    } 

    print "$0: [ssh ${user}\@${host} \"stats start -I ${id} ${statcmd}\"]";
    my(@start) = `ssh ${user}\@${host} "stats start -I ${id} ${statcmd}" 2>&1`;
    my($startcmd) = join(' ', @start);
    if ($startcmd){
	$startcmd =~ s/\s+//g;
	chomp($startcmd);
	if ($startcmd ne ""){
	   my($startmsg) = "starting stats collection for ${user}\@${host} $statcmd: " . $startcmd;
	   notify('warning', $startmsg);
	}
    }

    if ($#ret <= 2){
	my($bmsg) = "retrieving stats from ${user}\@${host} ${statcmd}: " . join(" ", @ret);
	notify('err', $bmsg);
    } else {
	shift(@ret);
	shift(@ret);
	shift(@ret);
	for (my $i=0;$i<=$#ret;$i++){
	    my($l) = $ret[$i];
	    chomp($l);
	    $l =~ s/^\s+//g;
	    $l =~ s/\s+$//g;
	    if ($l !~ /|$/){
		$l .= "|";
	    }
	    my(@cols) = ($id, $i, split(/\|/, $l));
	    for (my $j=0;$j<=$#cols;$j++){
		if ($cols[$j] =~ /\d+\.\-\d+/){
		    my($line) = join('|', @cols);
		    notify('debug', "${user}\@${host}: column ${j} of ${line} needed to be cleaned up, setting to 0");
		    $cols[$j] = 0;
		}
	    }
	    &$callback(@cols);
	}
    }

#Instance read_data read_latency read_ops write_data write_latenc write_ops other_ops other_latenc
#               b/s           ms       /s        b/s           ms        /s        /s           ms
#    vol0|0|0|0|6758|0.16|6|5121|0.00
#    vol1|5275648|0.86|322|1466368|2.10|154|0|0
#oradata2|0|0|0|0|0|0|0|0
#ctfs2008|0|0|0|0|0|0|8|0.00
#orabackup|0|0|0|0|0|0|0|0
#watsadmin|2604|0.00|1|61480|0.00|3|132|0.01
#ctfs2007|0|0|0|0|0|0|11|0.09
#itcomm_wo|0|0|0|0|0|0|0|0
#ctfs2009|0|0|0|0|0|0|4|0.00
#

}

1;
