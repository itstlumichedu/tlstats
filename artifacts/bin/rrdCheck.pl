#!/usr/local/bin/perl -w
# Copyright (C) 2000-2007 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.

use strict;
local $| = 1;

my($home) = $ENV{'HOME'};
require "${home}/bin/monitor.pl";

my($config);
$config->{'DEBUG'} = 0;

openlog('rrdC','cons,pid', 'user');
use File::Find;

my($offset) = 3600;

while(<>){
        chomp;
        next if (/(^$)|(^#)/);
        my($tt, $RRDlist, $DS, $min, $max, $severity, $minmsg, $maxmsg) = split(/\|/);
        my(@files);
	my(@treetops) = ($tt);

        find(sub { /${RRDlist}/i && push(@files, "$File::Find::dir/$_") }, @treetops);
        for my $RRD (@files){
                if (! -r $RRD){
                        notify('crit', "skipping: no such RRD [${RRD}]");
                        next;
                }
                if ($config->{'DEBUG'}){
                        print $RRD ."\n";
                }
                my($rrdinfo) = RRDs::info($RRD);
                my($lastupdate) = RRDs::last($RRD) - $offset;

                #complain if the last update wasn't after this time
                #currently
                my($maxCycles) = 2;
                my($rrdStep) = $$rrdinfo{'step'};
                my($now) = time;
                my($ago) = $now - $lastupdate;
                my($byThisTime) = ($now - ($maxCycles * $rrdStep)) - $offset;

                if ($lastupdate < $byThisTime){
                        if ($config->{'DEBUG'}){
                                notify($severity, "skipping: [${RRD}] lastUpdate ${ago} secs ago (step ${rrdStep})");
                        }
                        next;
                } else {
                        my ($start,$step,$names,$data) = RRDs::fetch($RRD, '-s', $lastupdate, '-e', $lastupdate, 'MAX');
			if (my $error = RRDs::error()){
			    notify('err', "RRDs::fetch failed, skipping $RRD: ${error}");
			    next;
			}

                        my(%nameIndex);
                        my(@nameArray) = @$names;

                        for (my $i=0;$i<=$#nameArray;$i++){
                                $nameIndex{$nameArray[$i]} = $i;
                        }

                        if (defined $nameIndex{$DS}){
                                my $line = $$data[0];
                                my $val = $$line[$nameIndex{$DS}];
                                if ($val){
                                        if ($min ne "" && $val <= $min){
                                                notify($severity, "${RRD} ${minmsg} (${DS} ${val} <= $min)");
                                        }
                                        if ($max ne "" && $val >= $max){
                                                notify($severity, "${RRD} ${maxmsg} (${DS} ${val} >= $max)");
                                        }
                                        if ($config->{'DEBUG'}){
                                                notify('debug', "(min|cur|max) = (${min}|${val}|${max}) [$RRD]");
                                        }
                                } else {
                                        if ($config->{'DEBUG'}){
                                                notify($severity, "[${RRD}] no value from DS (${DS}), skipping");
                                        }
                                        next;
                                }
                        } else {
                                if ($config->{'DEBUG'}){
                                        notify('crit', "[${RRD}] no such DS (${DS}), skipping");
                                }
                                next;
                        }
                }
        }
}

closelog();
