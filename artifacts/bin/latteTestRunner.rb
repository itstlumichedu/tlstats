# Run EUE tests on a single server.
# $HeadURL$
# $Id$
require 'selenium-webdriver'

#require 'rubygems'
#require 'selenium-webdriver-0.0.24'
#require '/opt/local/lib/ruby1.9/gems/1.9.1/gems/selenium-webdriver-0.0.24.gem'

def printStep(msg,start,stop)
  print msg.gsub(" ","_").downcase,":\t",stop-start,"\n"
end

#CTools EUE works by recording the timestamps for the beginning and ending of actions.
#As the Latte check step already does this, we will screen scrape and just send the 
#resulting time to the the printStep function
class EUERunner
	def self.runTest(server)
		begin

			# Create a wait condition that can be referenced later on in the script.
			wait = Selenium::WebDriver::Wait.new(:timeout => 5) # seconds

			driver = Selenium::WebDriver.for(:remote, :desired_capabilities => :htmlunit, :url => "http://localhost:4444/wd/hub")
			#driver = Selenium::WebDriver.for(:remote, :desired_capabilities => :firefox, :url => "http://localhost:4444/wd/hub")

			print "****STARTING TRANSACTION****\n"
			# Debugging 
			print "Server name: #{server}\n"

			# Step 1, get the Check URL for a specific server.
			driver.get "https://#{server}/check"

			wait.until {
				timeStampElement = driver.find_element(:xpath, "//body[contains(normalize-space(), 'elapsed')]") 
				}

			# Grab all text from the returned page.  Sample text is:
			# status=OK elapsed=0.543 server=skylark.dsc.umich.edu		
			allText = driver.find_element(:xpath, "//body[contains(normalize-space(), 'elapsed')]").text

			# Slice off the portion pertaining to the elapsed time and convert to a float
			elapsedTime = allText.slice!(18,6).to_f


			  #printStep("Check URL",start,Time.now())
			printStep("Check URL",0,elapsedTime)

			driver.quit

		# Generic error handling function.
		rescue => e

			driver.close
			#print e.message + "\n"
			print "We received an error.\n\n\n"

		end
	end
end
