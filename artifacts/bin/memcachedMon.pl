#!/usr/local/bin/perl -w

use strict;
local $| = 1;

use RRDs;

require '/ctstats/bin/monitor.pl';

my($config);
$config->{'STEP'} = 60;
$config->{'DEBUG'} = 0;
$config->{'program'} = 'memcachedMon';
$config->{'version'} = "0.01";
$config->{'homeDir'} = $ENV{'HOME'};
$config->{'libDir'} = $config->{'homeDir'} . "/lib";
$config->{'mibDir'} = $config->{'libDir'} . "/mibs";
$config->{'dataDir'} = $config->{'homeDir'} . "/data/memcached";
$config->{'timeout'} = 15;
if (! defined $config->{'logfacility'}){
  $config->{'logfacility'} = 'user';
}

openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

use lib qw($config->{'libDir'});

use Cache::Memcached;

my(%rrdTypes) = (
    misc => {
	"bytes" => "COUNTER",
	"curr_conns" => "GAUGE",
	"conn_structures" => "GAUGE",
	"pointer_size" => "GAUGE",
	"time" => "COUNTER",
	"total_items" => "GAUGE",
	"cmd_set" => "COUNTER",
	"accepting_conns" => "GAUGE",
	"bytes_written" => "COUNTER",
	"evictions" => "GAUGE",
	"curr_items" => "GAUGE",
	"pid" => "ABSOLUTE",
	"limit_maxbytes" => "ABSOLUTE",
	"uptime" => "COUNTER",
	"rusage_user" => "GAUGE",
	"cmd_get" => "COUNTER",
	"rusage_system" => "GAUGE",
	"get_hits" => "COUNTER",
	"bytes_read" => "COUNTER",
	"threads" => "GAUGE",
	"total_conns" => "COUNTER",
	"get_misses" => "COUNTER",
	"lsn_disabled_num" => "GAUGE",
	"cmd_flush" => "COUNTER",
    },
    malloc => {
	"total_free" => "GAUGE",
	"releasable_space" => "GAUGE",
	"free_chunks" => "GAUGE",
	"fastbin_blocks" => "GAUGE",
	"arena_size" => "GAUGE",
	"total_alloc" => "COUNTER",
	"max_total_alloc" => "GAUGE",
	"mmapped_regions" => "GAUGE",
	"mmapped_space" => "GAUGE",
	"fastbin_space" => "GAUGE",
    },
);

my(%farm);
my(@serverList);
while(<>){
    next if (/(^$|^#)/);
    chomp;
    my($host, $addr) = split(/\|/);
    $farm{$addr} = $host;
    push(@serverList, $addr);
}

my($servers) = \@serverList;

if ($servers && $#$servers >= 0){
} else {
    my($errmsg) = ($#$servers + 1) . " servers to query, exiting";
    notify('debug', $errmsg);
    exit;
}

my $memd = new Cache::Memcached {
    'servers' => $servers,
    'debug' => 0,
    'compress_threshold' => 10_000,
};

my($stats) = $memd->stats();
my(@ts) = ('misc', 'malloc');
for my $r (@ts){
    for my $s (keys %{$stats->{'hosts'}}){
	for my $v (keys %{$stats->{'hosts'}->{$s}->{$r}}){
	    my($dispv) = $v;
	    $dispv =~ s/listen/lsn/g;
	    $dispv =~ s/connection/conn/g;
	    if ($v ne "version"){
		my($val) = $stats->{'hosts'}->{$s}->{$r}->{$v};
		if ($val != 0 && defined $farm{$s}){
		    my($sval) = $s . " " . $r . " " . $dispv . ":" . $val;
		    notify('debug', $sval);
		    my($h) = $farm{$s};
		    my(@rv) = ($r, $h, $dispv, $val);
		    if ($config->{'DEBUG'}){
			notify('debug', "$r $h $dispv $val $rrdTypes{$r}{$dispv}");
		    } else {
			update(@rv);
		    }
		}
	    }
	}
    }
}

sub update {
    my($type) = shift(@_);
    my($hostname) = shift(@_);
    my($RRD);
    my($STEP) = $config->{'STEP'};

    my($name);
    if ($type eq "misc" || $type eq "malloc"){
	$name = shift(@_);
	$name =~ s/\s+/_/g;
	$RRD = $config->{'dataDir'} . "/" . $type . "/" . $hostname . "-" . $name . ".rrd";
    }
    my(@vals) = @_;

    my($t) = time;
    $t = int($t);

    for (my $i=0;$i<=$#vals;$i++){
	if (! $vals[$i] || $vals[$i] eq ''){
	    $vals[$i] = 0;
	}
	my $timeval = $vals[$i];
	chomp($timeval);
	if ($timeval =~ /\d*\:\d+\:\d+/){
	    $timeval =~ s/(\d*)\:(\d+)\:(\d+)//;
	    my ($hrs, $mins, $secs) = ($1, $2, $3);
	    $vals[$i] = $hrs * 3600 + $mins * 60 + $secs;
	}
    }

    if (! -e $RRD){
	my($START) = time - (2 * $STEP);
  
	notify('info', "Creating $RRD with step ${STEP} starting at $START");
	my($v, $msg) = RRD_create($RRD, $START, $STEP, $type, $name);
	if ($v){
	    notify('err', "couldn't create $RRD because $msg");
	    return;
	} 
    } 

    my($rv, $errmsg) = updateRRD($RRD, $t, @vals);
    if ($rv){
	notify('err', "error updating $RRD : ${errmsg}");
    }
}

sub RRD_create {
    my($RRD, $START, $interval, $type, $n) = @_;
    my(@dses);
    my(@rras) = (
		 "RRA:AVERAGE:0.5:1:43800",
		 "RRA:MAX:0.5:1:43800",
		 "RRA:AVERAGE:0.5:5:3000",
		 "RRA:MAX:0.5:5:3000",
		 "RRA:AVERAGE:0.5:10:5000",
		 "RRA:MAX:0.5:10:5000",
		 "RRA:AVERAGE:0.5:1440:732",
		 "RRA:MAX:0.5:1440:732"
 		 );

    if ($type eq "misc" || $type eq "malloc"){
	if (defined $rrdTypes{$type}{$n}){
	    my($typ) = $rrdTypes{$type}{$n};
	    @dses = ("DS:val0:${typ}:1200:U:U");
	} else {
	    return(1, "no RRD data type defined (passed in |${type}|${n}|)");
	}
    } else {
	notify('ERR', "could not create RRD of type ${type}");
	return(1, "do not recognize type ${type}");
    }


    RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses, @rras);

    if (my $error = RRDs::error()) {
	return(1, "Cannot create $RRD: $error");
    } else {
	return(0, "$RRD");
    }
}
