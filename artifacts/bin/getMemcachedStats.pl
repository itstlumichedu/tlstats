#!/usr/local/bin/perl -w

use strict;
local $| = 1;

use Cache::Memcached;
use Data::Dumper;

my(@serverList) = ("141.211.253.127:11211", "141.211.253.128:11211");
my($servers) = \@serverList;

my $memd = new Cache::Memcached
 {
    'servers' => $servers,
    'debug' => 0,
    'compress_threshold' => 10_000,
};
#$memd->set_servers(\@servers);

#$memd->set_compress_threshold(10_000);
#$memd->enable_compress(0);

#print Dumper($memd->stats());

my($statsref) = $memd->stats();
#print Dumper($statsref);

my($stats) = $statsref;
my(@ts) = ('misc', 'malloc');
for my $r (@ts){
    for my $s (keys %{$stats->{'hosts'}}){
	for my $v (keys %{$stats->{'hosts'}->{$s}->{$r}}){
	    my($dispv) = $v;
	    $dispv =~ s/listen/lsn/g;
	    $dispv =~ s/connection/conn/g;
	    if ($v ne "version"){
		my($val) = $stats->{'hosts'}->{$s}->{$r}->{$v};
		print $s . " " . $r . " " . $dispv . ":" . $val . "\n";
	    }
	}
    }
}


# $VAR1 = {
#     'hosts' => {
# 	'141.211.253.127:11211' => {
# 	    'misc' => {
#                                                                 'bytes' => '29808',
#                                                                 'curr_connections' => '5',
#                                                                 'connection_structures' => '6',
#                                                                 'pointer_size' => '32',
#                                                                 'time' => '1244728956',
#                                                                 'total_items' => '352',
#                                                                 'cmd_set' => '352',
#                                                                 'accepting_conns' => '1',
#                                                                 'bytes_written' => '67497',
#                                                                 'evictions' => '0',
#                                                                 'curr_items' => '138',
#                                                                 'pid' => '17878',
#                                                                 'limit_maxbytes' => '536870912',
#                                                                 'uptime' => '854295',
#                                                                 'rusage_user' => '0.037994',
#                                                                 'cmd_get' => '842',
#                                                                 'rusage_system' => '0.111982',
#                                                                 'version' => '1.2.8',
#                                                                 'get_hits' => '490',
#                                                                 'bytes_read' => '110078',
#                                                                 'threads' => '2',
#                                                                 'total_connections' => '22',
#                                                                 'get_misses' => '352',
#                                                                 'listen_disabled_num' => '0',
#                                                                 'cmd_flush' => '3'
# 								},
# 								    'malloc' => {
#                                                                   'total_free' => '107088',
#                                                                   'releasable_space' => '106296',
#                                                                   'free_chunks' => '3',
#                                                                   'fastbin_blocks' => '0',
#                                                                   'arena_size' => '311296',
#                                                                   'total_alloc' => '204208',
#                                                                   'max_total_alloc' => '0',
#                                                                   'mmapped_regions' => '14',
#                                                                   'mmapped_space' => '13897728',
#                                                                   'fastbin_space' => '0'
# 								  }
# 	}
#     },
#     'self' => {},
#     'total' => {
#                        'malloc_releasable_space' => 106296,
#                        'bytes' => 29808,
#                        'connection_structures' => 6,
#                        'total_items' => 352,
#                        'malloc_mmapped_regions' => 14,
#                        'malloc_free_chunks' => 3,
#                        'malloc_total_alloc' => 204208,
#                        'cmd_set' => 352,
#                        'bytes_written' => 67497,
#                        'malloc_fastbin_blocs' => 0,
# 
