# Run EUE tests on a single server.
# $HeadURL$
# $Id$
require 'selenium-webdriver'

#require 'rubygems'
#require 'selenium-webdriver-0.0.24'
#require '/opt/local/lib/ruby1.9/gems/1.9.1/gems/selenium-webdriver-0.0.24.gem'

def printStep(msg,start,stop)
  print msg.gsub(" ","_").downcase,":\t",stop-start,"\n"
end

class EUERunner
	def self.runTest(server)
		begin

			# Create a wait condition that can be referenced later on in the script.
			wait = Selenium::WebDriver::Wait.new(:timeout => 5) # seconds

			driver = Selenium::WebDriver.for(:remote, :desired_capabilities => :htmlunit, :url => "http://localhost:4444/wd/hub")
			#driver = Selenium::WebDriver.for(:remote, :desired_capabilities => :firefox, :url => "http://localhost:4444/wd/hub")

			print "****STARTING TRANSACTION****\n"
			# Debugging 
			print "Server name: #{server}\n"

			# Step 1, get the gateway page for a specific server.
			start = Time.now()
			  driver.get "https://#{server}/portal"
			  wait.until { driver.find_element(:xpath, "//span[normalize-space()='Course and Project Sites'] | //a[normalize-space()='University of Michigan']") }
			  printStep("Main page",start,Time.now())

			# Step 2, now get the xlogin page
			start = Time.now()
			  driver.get "https://#{server}/portal/xlogin"
			  wait.until { driver.find_element(:id, "submit") }
			  printStep("Xlogin page",start,Time.now())

			# Step 3, log in
			driver.find_element(:id, 'eid').send_keys("ctoolsresponsetimes@gmail.com")
			driver.find_element(:id, 'pw').send_keys("ct_performance")

			start = Time.now()
			  driver.find_element(:name, 'submit').click
			  
			  # Switched for 2.9N to accommodate the dashboard.
			  #wait.until { driver.find_element(:xpath, "//h2[contains(normalize-space(), 'From the CTools Team')]") }
			  driver.switch_to().frame(0)
			  wait.until { driver.find_element(:xpath, "//h3[contains(normalize-space(), 'Recent Activity')]") }
			  driver.switch_to().default_content();			  
			  # end of change
			  
#### These 4 lines are from ctstatsnp
#			  # Switched for 2.9N to accommodate the dashboard.
#			  #wait.until { driver.find_element(:xpath, "//h2[contains(normalize-space(), 'From the CTools Team')]") }
#			  wait.until { driver.find_element(:xpath, "//h2[contains(normalize-space(), 'Dashboard')] | //h2[contains(normalize-space(), 'From the CTools Team')]")}
#			  # end of change

			  printStep("Login",start,Time.now())

			# Step 4, open a site
			start = Time.now()
			  driver.find_element(:xpath, "//span[normalize-space()='End User Experience']").click
			  
			  # Switch to the frame that contains the MWS content.
			  driver.switch_to().default_content()
			  driver.switch_to().frame(0)

			  # Switch to the frame containing the site description, this has an index of 0 as of CTools 2.9.0A
			  #This line existed to accomodate the old web content tool, which used to be part of the main page
			  #for every site.  As of sakai 2.9.2 this is no longer the case.  So this frame switching is no longer
			  #necessary.
			  #driver.switch_to().frame "wciframe"
			  wait.until { driver.find_element(:xpath, "//div[starts-with(normalize-space(),'Test site that exists')]") }
			  printStep("Open Site",start,Time.now())
			  driver.switch_to().default_content()

			# Step 5, open the resources tool
			start = Time.now()
			  driver.find_element(:xpath, "//a/span[normalize-space()='Resources']").click
			  driver.switch_to().default_content()
			  driver.switch_to().frame(0)
			  wait.until { driver.find_element(:xpath, "//div[contains(normalize-space(), 'End User Experience Resources')]") }
			  printStep("Resources Tool",start,Time.now)

			# Get the original window handle.
			# We will switch back to this later after closing the pop-up containing a picture.
			mainWindowHandle = driver.window_handle
			#print "main handle: " + mainWindowHandle + "\n"
			#puts driver.window_handles.length
			#print "\n"

			# Step 6, open a file.
			start = Time.now()
			  driver.find_element(:xpath, "//a[normalize-space()='Bachalpsee.jpg']").click

				# wait until there is more than one window handle
			  wait.until { driver.window_handles.length == 2 }
	
				handles = driver.window_handles
				handles.delete(mainWindowHandle)

			  pictureWindowHandle = handles.last
				#print "new handle: " + pictureWindowHandle + "\n"
				driver.switch_to().window(pictureWindowHandle)

			  # How do we verify the download of the image?
			  #outFile = File.new "out.txt", "w"
			  #outFile.puts(driver.page_source())
			  #outFile.close
			  # This class is specific to the image
			  #wait.until { driver.find_element(:class, "decoded") }
			  printStep("Open File", start,Time.now())

			# Step 7, Close the new window.
			driver.close
			#For debugging purposes:
			#print driver.window_handles

			# Step 8, switch back to the primary frame in the original windows.
			driver.switch_to().window(driver.window_handles.last)
			driver.switch_to().default_content()

			# Step 9, log out.
			start = Time.now()
			driver.find_element(:xpath, "//a[@title='Logout']").click
			# Wait until the log out button on the gateway exists.
			wait.until { driver.find_element(:id, "eid") }
                        printStep("Log Out", start,Time.now())

			print "****ENDING TRANSACTION****\n\n\n"

			# Step 10, close the browser
			# Quitting actually stops the session and retrieves memory,
			# whereas close simply closes the browser window and leaves
			# the webDriver session running.
			driver.quit

		# Generic error handling function.
		rescue => e

			driver.close
			#print e.message + "\n"
			print "We received an error.\n\n\n"

		end
	end
end
