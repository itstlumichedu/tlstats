#!/usr/bin/perl -w
#
# Copyright (C) 2000-2009 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats about Sun Ultrasparc T1/T2
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

use strict;
local $| = 1;

use RRDs;
use FindBin;

my $homeDir = ${FindBin::Bin}."/..";
require "$homeDir/bin/monitor.pl";


#require '/ctstats/bin/monitor.pl';

my($config);
$config->{'STEP'} = 60;
$config->{'DEBUG'} = 0;
$config->{'program'} = 'coreMon';
$config->{'version'} = "0.01";

#$config->{'homeDir'} = $ENV{'HOME'};
$config->{'homeDir'} = $homeDir;

$config->{'libDir'} = $config->{'homeDir'} . "/lib";
$config->{'mibDir'} = $config->{'libDir'} . "/mibs";

#print "**** HACKING DATA DIR in $0 ****\n";
#$config->{'dataDir'} = $homeDir . "/data";
#$config->{'dataDir'} = "/usr/local/ctools/tmp/data";
#$config->{'dataDir'} = $config->{'homeDir'} . "/data/system";
$config->{'dataDir'} = $sharedDataDir;

$config->{'timeout'} = 15;
if (! defined $config->{'logfacility'}){
  $config->{'logfacility'} = 'user';
}

openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

use lib qw($config->{'libDir'});

use strict;
local $| = 1;

use SNMP_util;
snmpLoad_OID_Cache($SNMP_util::CacheFile);
$SNMP_Session::suppress_warnings = 2;

opendir(DIR, $config->{'mibDir'}) || die "can't opendir $config->{'mibDir'}: $!";
my(@files) =readdir(DIR);
closedir DIR;

for my $mib (@files){
    chomp($mib);
    $mib = $config->{'mibDir'} . "/" . $mib;
    next if ($mib =~ /^\./); #skip dot files
    next unless ($mib =~ /\.mib/); #skip any files that don't have mib in their name
    my($rv) = &snmpQueue_MIB_File($mib);
    if (! $rv && $config->{'DEBUG'}){
	print STDERR "* ERROR: Could not load $mib: $rv\n";
    }
}

my($hostname);
my(@corecols) = (
		 "extOutput",
	     );

sub coreupdate {
    update('core', @_);
}

sub update {
    my($type) = shift(@_);
    my($first) = shift(@_);
    my($STEP) = $config->{'STEP'};

    print "update: $0: ",join("|",@_),"\n";

    if ($type eq "core"){
	my($str) = shift(@_);
	my(@stats) = split(/\|/, $str);
	my($firststat) = shift(@stats);
	my($secondstat) = shift(@stats);

	next unless ($firststat eq "usr" && $secondstat eq "sys");
	my($cores) = $#stats / 2;

	my($coresysdir) = $config->{'dataDir'} . "/" . $type . "/" . $hostname;
	if (! -d "${coresysdir}"){
	    if(! mkdir "${coresysdir}", 0750){
		notify('crit', "Could not create dir ${coresysdir} ($!), skipping");
		next;
	    }
	}

	my($t) = time;
	$t = int($t);

	for (my $core=0;$core<=$cores;$core++){
	    my $RRD = $coresysdir . "/core" . $core . ".rrd";
	    if (! -e $RRD){
		my($START) = time - (2 * $STEP);
  		notify('info', "Creating $RRD with step ${STEP} starting at $START");

		if (! ensureRRDDirectory($RRD)) {
		  notify('crit', "Could not create ${RRD} ($!), skipping");
		}
		print "$0: creating new rrd: $RRD\n";
		my($v, $msg) = RRD_create($RRD, $START, $STEP, $type);
		if ($v){
		    notify('err', "couldn't create $RRD because $msg");
		    return;
		} 
	    } 

	    my($usr) = shift(@stats);
	    my($sys) = shift(@stats);
	    my(@vals) = ($usr, $sys);
	    print "$0: updating rrd: $RRD\n";
	    my($rv, $errmsg) = updateRRD($RRD, $t, @vals);
	    if ($rv){
		notify('err', "error updating $RRD : ${errmsg}");
	    }
	}
    }
}

sub RRD_create {
    my($RRD, $START, $interval, $type) = @_;
    my(@dses);
    my(@rras) = (
		 "RRA:AVERAGE:0.5:1:43800",
		 "RRA:MAX:0.5:1:43800",
		 "RRA:AVERAGE:0.5:5:3000",
		 "RRA:MAX:0.5:5:3000",
		 "RRA:AVERAGE:0.5:10:5000",
		 "RRA:MAX:0.5:10:5000",
		 "RRA:AVERAGE:0.5:1440:732",
		 "RRA:MAX:0.5:1440:732"
 		 );

    if ($type eq "core"){
	@dses = (
		    "DS:usr:GAUGE:1200:U:U",
		    "DS:sys:GAUGE:1200:U:U",
	     );
    } else {
	notify('ERR', "could not create RRD of type ${type}");
	return(1, "do not recognize type ${type}");
    }


    RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses, @rras);

    if (my $error = RRDs::error()) {
	return(1, "Cannot create $RRD: $error");
    } else {
	return(0, "$RRD");
    }
}


while(<>){
    chomp;
    next if (/(^$)|(^#)/);
    my($community, $ip);
    ($hostname, $community, $ip) = split(/\|/);
    my($agent) = $community . "\@" . $ip;

    my(%sets);
    $sets{"core"}{"cols"} = \@corecols;
    $sets{"core"}{"func"} = \&coreupdate;

    notify('debug', "starting collection for ${hostname}");
    for my $set (keys %sets){
	if (defined ${sets{$set}{"exclude"}} ){
    	   next if ($hostname =~ /${sets{$set}{"exclude"}}/);
        }
        notify('debug', "starting ${set} collection for ${hostname}");
	my($rv) = &snmpmaptable($agent, $sets{$set}{"func"}, { 'use_getbulk' => 0 }, @{$sets{$set}{"cols"}});
        if (!defined($rv) || $rv < 0){
   	  notify('err', "getting ${set} from ${hostname}");
        } else {
          notify('debug', "collected ${rv} rows in ${set} collection for ${hostname}");
        }
    }
    notify('debug', "ending collection for ${hostname}");
}

