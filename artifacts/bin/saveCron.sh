#!/bin/bash
# save the crontab of the current user with a current date
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/saveCron.sh $
# $Id: saveCron.sh 1693 2012-05-15 18:26:23Z dlhaines $
set -u
set -e
crontab -l >| crontab.cron.$(date +"%Y%m%d%H%M")
#end
