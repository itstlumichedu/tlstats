#!/usr/local/bin/perl -w

my($LIB) = $ENV{'HOME'} . "/lib";
use lib qw($LIB);

use strict;
local $| = 1;
my($DEBUG) = 0;

sub usage () {
    die "Usage: $0 host community\n";
}

use SNMP_util;
## Override the default cache file ("OID_cache.txt") with "cache_test.txt"
#$SNMP_util::CacheFile = "cache_test.txt";
snmpLoad_OID_Cache($SNMP_util::CacheFile);

my($MIBdir) = $LIB . "/mibs";

opendir(DIR, $MIBdir) || die "can't opendir $MIBdir: $!";
my(@files) =readdir(DIR);
closedir DIR;

for my $mib (@files){
    chomp($mib);
    $mib = $MIBdir . "/" . $mib;
    next if ($mib =~ /^\./); #skip dot files
    next unless ($mib =~ /\.mib/); #skip any files that don't have mib in their name
    my($rv) = &snmpQueue_MIB_File($mib);
#  my($rv) = &snmpMIB_to_OID($mib);
    if (! $rv && $DEBUG){
	print STDERR "* ERROR: Could not load $mib: $rv\n";
    }
}

#my($agent) = "watsops\@10.211.253.193";
my($agent) = "watsops\@141.211.140.29";
#my($agent) = "watsops\@141.211.253.194";
#my($table) = "totalClientConnections.0";
#my($table) = "vserverName";
#my @ret = &snmpwalk($agent, $table);
#for my $desc (@ret) {
#    my ($oid, $d) = split(':', $desc, 2);
#    print "$d\n";
#}

sub printfun {
    shift(@_);
    print join('|', @_) . "|\n";
}

my(@vservercols) = (
	     "vsvrName", 
	     "vsvrIpAddress",
	     "vsvrTotalRequests",
	     "vsvrTotalResponses",
	     "vsvrTotalRequestBytes",
	     "vsvrTotalResponseBytes",
	     "vsvrTotalPktsRecvd",
	     "vsvrTotalPktsSent",
	     "vsvrTotalSynsRecvd",
  	     "lbvsvrAvgSvrTTFB",
	     "lbvsvrActiveConn",
	     );

my(@svcgrpcols) = (
		   "svcGrpMemberName",
		   "svcGrpMemberAvgTransactionTime",
		   "svcGrpMemberEstablishedConn",
		   "svcGrpMemberActiveConn",
		   "svcGrpMemberSurgeCount",
		   "svcGrpMemberTotalRequests",
		   "svcGrpMemberTotalResponses",
		   "svcGrpMemberTotalRequestBytes",
		   "svcGrpMemberTotalResponseBytes",
		   "svcGrpMemberAvgSvrTTFB",
		   "svcGrpMemberTotalPktsRecvd",
		   "svcGrpMemberTotalPktsSent",
		   "svcGrpMemberTotalSynsRecvd",
		   );


my(@cols) = (
	     "vsvrName", 
	     "vsvrIpAddress",
#	     "vsvrAvgTransactionTime",
	     "vsvrTotalRequests",
#	     0,
	     "vsvrTotalResponses",
	     "vsvrTotalResponses",
	     "vsvrTotalRequestBytes",
	     "vsvrTotalRequestBytes",
	     "vsvrTotalResponseBytes",
	     "vsvrTotalResponseBytes",
	     "vsvrTotalPktsRecvd",
	     "vsvrTotalPktsRecvd",
	     "vsvrTotalPktsSent",
	     "vsvrTotalPktsSent",
	     "vsvrTotalSynsRecvd",
	     "vsvrTotalSynsRecvd",
	     );

my(@svcols) = (
	       "svcServiceName",
	       "svcIpAddress",
	       "svcMaxReqPerConn",
	       "svcAvgTransactionTime",
	       "svcEstablishedConn",
	       "svcActiveConn",
	       "svcSurgeCount",
	       "svcTotalRequests",
	       "svctotalJsTransactions",
	       "svcTotalResponses",
	       "svctotalJsTransactions",
	       "svcTotalRequestBytes",
	       "svctotalJsTransactions",
	       "svcTotalResponseBytes",
	       "svctotalJsTransactions",
	       "svcTotalPktsRecvd",
	       "svctotalJsTransactions",
	       "svcTotalPktsSent",
	       "svctotalJsTransactions",
	       "svcTotalSynsRecvd",
	       "svctotalJsTransactions",
#	       "servicePort",
#	       "serviceType",
#	       "serviceState",
    );
my(@nsvcols) = (
	       "svcServiceName",
	       "svcIpAddress",
#	       "svcServicePort",
#	       "svcServiceType",
#	       "svcServiceState",
	       "svcMaxReqPerConn",
	       "svcAvgTransactionTime",
	       "svcEstablishedConn",
	       "svcActiveConn",
	       "svcEstablishedConn",
	       "svcSurgeCount",
	       "svcTotalRequests",
	       "svctotalJsTransactions",
	       "svcTotalResponses",
	       "svcTotalResponses",
	       "svcTotalRequestBytes",
	       "svcTotalRequestBytes",
	       "svcTotalResponseBytes",
	       "svcTotalResponseBytes",
	       "svcTotalPktsRecvd",
	       "svcTotalPktsRecvd",
	       "svcTotalPktsSent",
	       "svcTotalPktsSent",
	       "svcTotalSynsRecvd",
	       "svcTotalSynsRecvd",
	       );

my(@vsvcols) = (
		"serviceHitsLow",
		"serviceHitsHigh",
		"servicePersistentHitsLow",
		"servicePersistentHitsHigh",
		);


my(@vsvsuppcols) = (
	     "vsvrName", 
	     "vsvrIpAddress",
#	     "vsvrAvgTransactionTime",
		    "lbvsvrAvgSvrTTFB",
	     "vsvrMaxReqPerConn",
	     "vsvrCurClntConnections",
	     "vsvrCurSrvrConnections",
	     "vsvrSurgeCount",
		    );

my(@sslcols) = (
"sslTotTransactions",
"sslTotNULLAuthorizations",
"sslTotSSLv2Transactions",
"sslTotNULLAuthorizations",
"sslTotSSLv3Transactions",
"sslTotNULLAuthorizations",
"sslTotTLSv1Transactions",
"sslTotNULLAuthorizations",
"sslTotSessionHits",
"sslTotNULLAuthorizations",
"sslTotSSLv2Sessions",
"sslTotNULLAuthorizations",
"sslTotSSLv3Sessions",
"sslTotNULLAuthorizations",
"sslTotTLSv1Sessions",
"sslTotNULLAuthorizations",
"sslTotExpiredSessions",
"sslTotNULLAuthorizations",
"sslTotNewSessions",
"sslTotNULLAuthorizations",
"sslTotSessionHits",
"sslTotSessionMiss",
		);

my(@syscols) = (
#"tcpTotClientConnOpened",
#"tcpTotServerConnOpened",
"tcpErrSentRst",
"tcpReuseHit",
"tcpErrStrayPkt",
"tcpTotZombieCltConnFlushed",
"tcpTotZombieSvrConnFlushed",
"tcpCurPhysicalServers",
#"tcpErrCookiePktSeqReject",
#"tcpErrCookiePktSigReject",
"nsCPUusage",
"tcpErrSynGiveUp",
"httpTotRequests",
"httpTotGets",
"httpTotPosts",
"httpTotResponses",
"httpTotClenResponses",
"httpTotChunkedResponses",
"httpErrNoreuseMultipart",
"httpErrIncompleteHeaders",
"httpErrIncompleteRequests",
"httpErrIncompleteResponses",
"httpTot11Responses",
"httpErrServerBusy",
#"totudpsessions",
	       );

my(@aclcols) = (
"aclName",
"aclPriority",
"aclperHits",
		);

my(@nsbase) = (
	       "sysBuildVersion",
	       );

my $srows;
#print STDERR "vservercols\n";	       
#my($nrows) = &snmpmaptable($agent, \&printfun, @vservercols);
#my($srows) = &snmpmaptable($agent, \&printfun, @svcols);

#$srows = &snmpmaptable($agent, \&printfun, @aclcols);
#$srows = &snmpmaptable($agent, \&printfun, @cols);
#$srows = &snmpmaptable($agent, \&printfun, @nsbase);

$srows = &snmpmaptable($agent, \&printfun, @svcgrpcols);

#print STDERR "sslcols\n";	       
#my($sslrows) = &snmpmaptable($agent, \&printfun, @sslcols);
#print STDERR "syscols\n";	       
#my($sysrows) = &snmpmaptable($agent, \&printfun, { 'use_getbulk' => 0 }, @syscols);

#okay
#my($vsv) = &snmpmaptable($agent, \&printfun, @vsvcols);

#my($vsvsupp) = &snmpmaptable($agent, \&printfun, @vsvsuppcols);
