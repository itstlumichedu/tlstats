#!/bin/bash
MUNIN_DIR="/ctstats/data/sakai/CTLOAD"

for f in `find ${MUNIN_DIR} -name '*.xml' -print` ;
do
f_rrd=`dirname ${f}`/`basename ${f} .xml`.rrd
mv -f "${f_rrd}" "${f_rrd}.bak"
#chown root:0 "${f_rrd}.bak"
rrdtool restore "$f" "${f_rrd}"
#chown munin:munin "${f_rrd}"
done
