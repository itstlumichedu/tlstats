#!/usr/local/bin/perl -w

my($LIB) = ${HOME} . "/lib";
use lib qw($LIB);

use strict;
local $| = 1;
my($DEBUG) = 0;

sub usage () {
    die "Usage: $0 host community\n";
}

use SNMP_util;
## Override the default cache file ("OID_cache.txt") with "cache_test.txt"
#$SNMP_util::CacheFile = "cache_test.txt";
$SNMP_Session::suppress_warnings = 1;
snmpLoad_OID_Cache($SNMP_util::CacheFile);

my($MIBdir) = $LIB . "/mibs";

opendir(DIR, $MIBdir) || die "can't opendir $MIBdir: $!";
my(@files) =readdir(DIR);
closedir DIR;

for my $mib (@files){
    chomp($mib);
    $mib = $MIBdir . "/" . $mib;
    next if ($mib =~ /^\./); #skip dot files
    next unless ($mib =~ /\.mib/); #skip any files that don't have mib in their name
    my($rv) = &snmpQueue_MIB_File($mib);
#  my($rv) = &snmpMIB_to_OID($mib);
    if (! $rv && $DEBUG){
	print STDERR "* ERROR: Could not load $mib: $rv\n";
    }
}

my($host) = @ARGV;
chomp($host);
#my($agent) = "public\@${host}:161:2.0:5:1.0:2";
my($agent) = "public\@${host}";

#my($table) = "jvmMemPoolPeakUsed";
#my @ret = &snmpwalk($agent, { 'use_getbulk' => 0 }, $table);
#for my $desc (@ret) {
#    my ($oid, $d) = split(':', $desc, 2);
#    print "$oid -> $d\n";
#
#    my($noid) = $table . "." . $oid;
#    my @ret = &snmpgetnext($agent, $noid);
#    foreach $desc (@ret) {
#	($oid, $desc) = split(':', $desc, 2);
#	print "$noid -> $oid = $desc\n";
#    }
#}

sub printfun {
    #shift(@_);
    my(@vals) = @_;
    for (my $i=0;$i <= $#vals; $i++){
	if (! $vals[$i] || $vals[$i] eq ''){
	    $vals[$i] = 0;
	}
    }
    #next if (! @vals);
    #next if (! $vals[0] || ! $vals[1]);
    print join('|', @vals) . "|\n";
}

my(@cols) = (
#             "jvmMemPoolName",
#             "jvmMemPoolInitSize",
#             "jvmMemPoolUsed",
#             "jvmMemPoolPeakUsed",
#
#
#	     "jvmClassesTotalLoadedCount",
#	     "jvmClassesUnloadedCount",
#	     "jvmMemoryHeapInitSize",
#	     "jvmMemoryHeapUsed",
#	     "jvmMemoryHeapCommitted",
#	     "jvmMemoryNonHeapInitSize",
#	     "jvmMemoryNonHeapUsed",
#	     "jvmMemoryNonHeapCommitted",
#	     "jvmThreadCount",
#	     "jvmThreadDaemonCount",
#	     "jvmThreadTotalStartedCount",
#	     "jvmRTUptimeMs",
#	     "jvmJITCompilerTimeMs",
#
#	     "jvmMemManagerName",
#	     "jvmMemGCCount",
#	     "jvmMemGCTimeMs",

	     "jvmThreadInstName",
	     "jvmThreadInstState",
	     "jvmThreadInstCpuTimeNs",
	     "jvmThreadInstWaitTimeMs",
	     "jvmThreadInstWaitCount",
	     "jvmThreadInstBlockTimeMs",
	     "jvmThreadInstBlockCount",
	     "jvmThreadInstLockName",

#	     "ssCpuRawUser",
#	     "ssCpuRawNice",
#	     "ssCpuRawSystem",
#	     "ssCpuRawIdle",
#	     "ssCpuRawWait",
#	     "ssCpuRawKernel",
#	     "ssRawInterrupts",
#	     "ssRawContexts",
 	     );

my(@cols2) = (
	     "memAvailSwap",
	     "memAvailReal",
	     "memShared",
	     "memBuffer",
	     "memCached",
	      );

my(@cols3) = (
	      "dskPath",
	      "dskDevice",
	      "dskAvail",
	      "dskUsed",
	      "dskPercentNode",
	      );

my(@diskiocols) = (
		   "diskIODevice",
		   "diskIONRead",
		   "diskIONWritten",
		   "diskIOReads",
		   "diskIOWrites",
		   );

my($nrows) = &snmpmaptable($agent, \&printfun, @diskiocols);
#my($nrows) = &snmpmaptable($agent, \&printfun, { 'use_getbulk' => 0 }, @diskiocols);
#($nrows) = &snmpmaptable($agent, \&printfun, { 'use_getbulk' => 0 }, @cols2);
print $nrows;

