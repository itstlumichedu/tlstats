#!/usr/bin/perl -w 

use strict;
local $| = 1;

#1168339752 alaking_restart
#1168512552 alaking_restart
#1168598045 alaking_restart
#1169116445 alaking_restart

print <<HEAD;
BEGIN:VCALENDAR
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-CALNAME;VALUE=TEXT:evt
VERSION:2.0
HEAD

while(<>){
    chomp;
    next if (/(^$)|(^#)/);
    my($tick, $descr) = split;
    $descr =~ s/\&/./g;
    $descr =~ s/\:/-/g;
    my($uid) = $tick . "\@" . "nagios.ds.itd.umich.edu";

    if ($tick =~ /\d+/ && $tick < time){		    
	my($start) = $tick;
	my($end) = $tick + 30;
	my($dtstart) = caltime($start);
	my($dtend) = caltime($end);

	print <<EVT;
BEGIN:VEVENT
DTSTART:${dtstart}
DTEND:${dtend}
SUMMARY: ${descr}
UID:${uid}
END:VEVENT
EVT

    }

}


print "END:VCALENDAR\n";


sub caltime {
    my($t) = @_;
    my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime($t);
    $year += 1900;
    $mon += 1;
    my($ts) = sprintf("%04d%02d%02dT%02d%02d%02dZ", $year, $mon, $mday, $hour, $min, $sec);
    return $ts;
}
