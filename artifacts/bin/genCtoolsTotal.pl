#!/usr/local/bin/perl -w

use strict;
local $| = 1;

use RRDs;

my(%periods) = (
  "day" => -86400,
  "week" => -604800,
  "month" => -2419200,
  "year" => -31622400,
		);

my($RRDdir) = "/afs/umich.edu/group/ct/ctlogs/ctools.umich.edu/data/users";
opendir(IN, "${RRDdir}") || die "could not open directory: $!";
my @dirlist = readdir(IN);
my($outDir) = "/afs/umich.edu/group/ct/ctlogs/ctools.umich.edu/public_html";

my(%aps);
for my $file (sort @dirlist){
    next unless ($file =~ /.rrd$/);
    my($ap) = $file;
    $ap =~ s/\.rrd$//g;
    $aps{$ap} = 1;
}

my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year += 1900;
$mon += 1;
my($timestamp) = sprintf("%4d-%02d-%02d %02d:%02d:%02d",
			 $year, $mon, $mday, $hour, $min, $sec);

my(@defs);
my($cdef) = "CDEF:total=";
my(@arrcdef);
my(@postfixcdef);
for my $ap (keys %aps){
    my($RRD) = "$RRDdir/${ap}.rrd";
    my($dap) = substr($ap, 0, 19);
    $dap =~ s/\.//g;
    push(@defs, "DEF:${dap}=${RRD}:users:AVERAGE");
    push(@arrcdef, "$dap,UN,0,$dap,IF");
    push(@postfixcdef, "+");
}
shift(@postfixcdef);

$cdef .= join(",", @arrcdef) . "," . join(",", @postfixcdef);

#print STDERR join("\n", @defs) . "\n";
#print STDERR $cdef . "\n";

#my($numaps) = scalar(keys %aps);
for my $period (sort keys %periods){
    my($outFile) = "total-${period}.png";
  my($averages,$xsize,$ysize) = RRDs::graph (
    "$outDir/$outFile",
    "--imgformat", "PNG",
    "--title", "Total users for Ctools over the last $period",
					     "--start", $periods{$period},
    "--lower-limit", 1,
    "--units-exponent", 0,
#    "--y-grid", "1:10",
    "--vertical-label", "users",
    @defs,
    $cdef,
    "LINE1:total#0000FF:Total users",
    "CDEF:totlim=total,UN,INF,UNKN,IF",
    'GPRINT:total:MIN:(min=%.0lf',
    'GPRINT:total:AVERAGE:ave=%.0lf',
    'GPRINT:total:MAX:max=%.0lf)',
    'COMMENT:\n',
    "AREA:totlim#FF0000:no results",
    'COMMENT:\n',
    "COMMENT:generated on ${timestamp} UTC\n",
    'COMMENT:\n',
					     );
    my($ERROR) = RRDs::error;
    if ($ERROR) {
	print STDERR "ERROR: $ERROR\n";
    }
}
