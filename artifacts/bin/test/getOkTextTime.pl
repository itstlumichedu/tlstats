#!/usr/bin/perl -w
# Script to time get of ok.txt or alternate file
# Requires host name as argument.

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/getOkTextTime.pl $
# $Id: getOkTextTime.pl 2146 2015-06-04 16:50:55Z dlhaines $

use strict;
my $TRACE=1;
my $DEBUG_DIR=".";
my $OUTPUTFILE="$DEBUG_DIR/DEBUG-getOkTextTime.TXT";
# time to use if the request failed.
my $FAILED_TIME=10;

# file to check
my $CHECK_FILE_SUFFIX="/access/content/public/ok.txt";
my $DEFAULT_HOST = "sail.dsc.umich.edu";

# Specify the max time to wait for a response.  Need to balance having
# a time long enough that a timeout doesn't look reasonable but short
# enough that tests that time out don't sit around so long that they
# hold up testing.

my $TIMEOUT=$FAILED_TIME;
open (MYFILE, ">>$OUTPUTFILE") if ($TRACE);

my $DATE;
#my $DATE=`date`;
#chomp($DATE);

my $rc;
my @m;
my ($system,$elapsed);

my $HOST;
# my $HOST = shift;
# $HOST=($HOST ? $HOST : $DEFAULT_HOST);

print MYFILE "gOTT: $DATE: HOST: [$HOST]\n" if ($TRACE);

# Method should set both system and elapsed to signal that it has
# found the data.  A system value of "FAILED" means that the file
# could not be retrieved.

## Try to get the standard "ok.txt" file.

sub tryOkFile {
    my @m;

    my $cmd = "/usr/bin/time -p wget --timeout=$TIMEOUT -q --no-check-certificate https://${HOST}/${CHECK_FILE_SUFFIX} -O - 2>&1";
    print "cmd: [$cmd]\n" if ($TRACE);
    @m= qx/$cmd/;
    $rc = $?;
    print MYFILE "rc A: $rc\n" if ($rc && $TRACE);    

    # make sure all the text is on a single line so it is easily parsed.
    chomp(@m);
    my $m = join(" ",@m);

    print "m: [$m]\n" if ($TRACE);

    ($system,$elapsed) = $m =~ m/ok-(\S+)\s*real\s+(\d+\.\d+)|/sm;
    print MYFILE "OKFILE: system: [$system] elapsed: [$elapsed]\n" if ($TRACE && defined($system));

    return ($system,$elapsed);
}

# Set values for failed queries.
sub setFailed {
  ($system,$elapsed)=("FAILED",$FAILED_TIME);
  
  if ($TRACE) {
    print "system:$system elapsed:$elapsed\n";
    print MYFILE "FAILED: system:$system elapsed:$elapsed FAILED\n";
  }

  return ($system,$elapsed);
}


sub main {
$DATE=`date`;
chomp($DATE);

$HOST = shift;
$HOST=($HOST ? $HOST : $DEFAULT_HOST);

print MYFILE "gOTT: $DATE: HOST: [$HOST]\n" if ($TRACE);


# Get file or set values for failure.
($system,$elapsed) = tryOkFile();
($system,$elapsed) = setFailed() unless($elapsed);

print MYFILE "system:$system elapsed:$elapsed\n" if ($TRACE);

#print "system:$system elapsed:$elapsed\n";
return ("system:$system elapsed:$elapsed");

# end main
}


### Run the script unless it is called from a test file.
print main(@ARGV) unless ( $0 =~ m|.*\.t| );

# Need to return 1 if called from a test file.
1;


#end
