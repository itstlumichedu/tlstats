#!/bin/bash
# update the freeMem urls in the appropriate range.
#sed 1,24s/manager\\/freeMem.jsp/ctadmin-jsp\\/freeMem.jsp/ tomcat.monitor.test

set -x


TS=$(date +"%Y%m%d%H%M");
echo $TS;

DIR=/usr/local/ctools/app/ctools/bin/test

ACTIVE_FILE=$DIR/tomcat.monitor.test
BCK_FILE=$ACTIVE_FILE.bkp

cp $ACTIVE_FILE $BCK_FILE

sed 1,24s/manager\\/freeMem.jsp/ctadmin-jsp\\/freeMem.jsp/ $BCK_FILE >| $ACTIVE_FILE
diff -u $BCK_FILE $ACTIVE_FILE

#end
