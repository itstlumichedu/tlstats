#!/bin/bash

# Move a drraw file to the HIDE directory

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/hideGraph.sh $
# $Id: hideGraph.sh 1564 2012-03-15 18:27:52Z dlhaines $

DRRAW_DATA_DIR="/usr/local/ctools/app/ctools/ctstats/cgi-bin/saved";

echo "RECENT CHANGES UNTESTED";
exit 1;

## Need to test that reference correct saved directories.
## Likely that won't be needed if permissions are set correctly in the saved directory.

set -u 
set -e

set -x

FILE=$1

ls -l $DRRAW_DATA_DIR/$1;
mv $DRRAW_DATA_DIR/$1 $DRRAW_DATA_DIR/HIDE;

#ls -l /usr/local/ctools/app/ctools/ctstats/cgi-bin/saved/$1
#mv /usr/local/ctools/app/ctools/ctstats/cgi-bin/saved/$1  /usr/local/ctools/app/ctools/ctstats/cgi-bin/saved/HIDE;

#end
