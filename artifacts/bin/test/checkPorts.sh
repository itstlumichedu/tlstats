#!/bin/bash

# Run check that can reach port on remote server

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/checkPorts.sh $
# $Id: checkPorts.sh 2146 2015-06-04 16:50:55Z dlhaines $

#set -x
# force error when see an unset variable.
set -u

TRACE=1;
TRIES=1;

function checkPort {

   result=$("$@");
   local rc=$?;

    if [ 0 -ne $TRACE ]; then
	echo -n "rc: [$rc] RESULT: [$result]";
    fi;

    if [ $rc -eq 0 ]; then
	echo "+++++++ ok";
    else 
	echo "------- not ok";
    fi;

}

function checkPortUdp {
    HOST=$1;
    PORT=$2;

    echo -en "###### check upd port access for [$HOST:$PORT]\t";

    CMD="nc -u -v -z  -w2 $HOST $PORT";
    
    checkPort $CMD;
}

function checkPortTcp {
    HOST=$1
    PORT=$2;

    echo -en "###### check tcp port access for [$HOST:$PORT]\t";

    CMD="nc -v -z  -w2 $HOST $PORT";
    
    checkPort $CMD;
}

function printHeader {
    echo "running" $(basename $0) "on" $(hostname) "at:" $(date);
}

function printReason {
    echo -e "\n**** checking $@";
}

function printSkip {
    echo "skipping inactive: [$@]";
}

printHeader

###############################

# CTIR ctstats servers
printReason "ctstats servers";
checkPortUdp fiesta.dsc.umich.edu 161;
checkPortUdp journey.dsc.umich.edu 161;

# CTIR load runner servers
printReason "CTIR load runner servers";
checkPortUdp neon.dsc.umich.edu 161
checkPortUdp newport.dsc.umich.edu 161

############ CTIR db instances (Oracle access)
printReason "CTIR Oracle port access";
checkPortTcp courser.dsc.umich.edu 1521
checkPortTcp galaxy.dsc.umich.edu 1521
checkPortTcp yeoman.dsc.umich.edu 1521

# CTIR db servers (snmp access)
printReason "CTIR SNMP for db servers";
checkPortUdp courser.dsc.umich.edu 161;
checkPortUdp galaxy.dsc.umich.edu 161;
checkPortUdp yeoman.dsc.umich.edu 161;

### This is the production db server.  Even if there is failover the
### new server will be called malibu and have the original malibu IP.
### There is no reason to explicitly monitor montana.
checkPortTcp malibu.dsc.umich.edu 1521;
checkPortUdp malibu.dsc.umich.edu 161;
#############   

# CTIR Cacti db instance.  The cacti instance is 
# not active.

#printReason "Cacti mysql connection";
#checkPortTcp webapps-db-dev.web.itd.umich.edu 3306

## CTIR ctdev
printReason "CTIR ctdev app servers";
checkPortUdp prefect.dsc.umich.edu 161;
checkPortUdp probe.dsc.umich.edu 161;

## CTIR ctqa
printReason "CTIR ctqa app servers";
checkPortUdp scorpio.dsc.umich.edu 161;
checkPortUdp special.dsc.umich.edu 161;
checkPortUdp squire.dsc.umich.edu 161;

## CTIR ctint
printReason "CTIR ctint app servers";
checkPortUdp taurus.dsc.umich.edu 161;
checkPortUdp tempo.dsc.umich.edu 161;

## CTIR load
printReason "CTIR ctload app servers";
checkPortUdp sail.dsc.umich.edu 161;
checkPortUdp senator.dsc.umich.edu 161;
checkPortUdp silverado.dsc.umich.edu 161;
checkPortUdp sonic.dsc.umich.edu 161;
checkPortUdp spark.dsc.umich.edu 161;
checkPortUdp spectrum.dsc.umich.edu 161;
checkPortUdp standard.dsc.umich.edu 161;
checkPortUdp suburban.dsc.umich.edu 161;
checkPortUdp special.dsc.umich.edu 161;

# not in cluster yet.
#checkPortUdp spectrum.dsc.umich.edu 161;

## CTIR prod
printReason "CTIR prod app servers";
checkPortUdp tahoe.dsc.umich.edu 161;
checkPortUdp tavera.dsc.umich.edu 161;
checkPortUdp tigra.dsc.umich.edu 161;
checkPortUdp tornado.dsc.umich.edu 161;
checkPortUdp tosca.dsc.umich.edu 161;
checkPortUdp townsman.dsc.umich.edu 161;
checkPortUdp tracker.dsc.umich.edu 161;
checkPortUdp traverse.dsc.umich.edu 161;

#### UMIAC

printReason "CTIR umiac ctint"
checkPortTcp umiacint.dsc.umich.edu 8888

checkPortTcp zephyr.dsc.umich.edu 8888
checkPortTcp zodiac.dsc.umich.edu 8888

checkPortUdp zephyr.dsc.umich.edu 161
checkPortUdp zodiac.dsc.umich.edu 161

printReason "CTIR umiac ctqa"
checkPortTcp umiacqa.dsc.umich.edu 8888

checkPortTcp bluebonnet.dsc.umich.edu 8888
checkPortTcp brilliant.dsc.umich.edu 8888

checkPortUdp bluebonnet.dsc.umich.edu 161
checkPortUdp brilliant.dsc.umich.edu 161

printReason "CTIR umiac ctdev"
checkPortTcp umiacdev.dsc.umich.edu 8888

checkPortTcp ranchero.dsc.umich.edu 8888
checkPortTcp roadster.dsc.umich.edu 8888

checkPortUdp ranchero.dsc.umich.edu 161
checkPortUdp roadster.dsc.umich.edu 161

printReason "CTIR umiac ctload"
checkPortTcp umiacload.dsc.umich.edu 8888

checkPortTcp satellite.dsc.umich.edu 8888
checkPortTcp shadow.dsc.umich.edu 8888
checkPortTcp sierra.dsc.umich.edu 8888
checkPortTcp spirit.dsc.umich.edu 8888
checkPortTcp sprinter.dsc.umich.edu 8888
checkPortTcp stealth.dsc.umich.edu 8888

checkPortUdp satellite.dsc.umich.edu 161
checkPortUdp shadow.dsc.umich.edu 161
checkPortUdp sierra.dsc.umich.edu 161
checkPortUdp spirit.dsc.umich.edu 161
checkPortUdp sprinter.dsc.umich.edu 161
checkPortUdp stealth.dsc.umich.edu 161

printReason "CTIR umiac prod"
checkPortTcp umiac.dsc.umich.edu 8888

checkPortTcp caliber.dsc.umich.edu 8888
checkPortTcp caravan.dsc.umich.edu 8888
checkPortTcp cavalier.dsc.umich.edu 8888
checkPortTcp challenger.dsc.umich.edu 8888
checkPortTcp charger.dsc.umich.edu 8888
checkPortTcp colt.dsc.umich.edu 8888

checkPortUdp caliber.dsc.umich.edu 161
checkPortUdp caravan.dsc.umich.edu 161
checkPortUdp cavalier.dsc.umich.edu 161
checkPortUdp challenger.dsc.umich.edu 161
checkPortUdp charger.dsc.umich.edu 161
checkPortUdp colt.dsc.umich.edu 161

#end
