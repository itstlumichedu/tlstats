#!/bin/bash

# Run simple snmpget on known host so can see if snmp works from command line.

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/checkSnmp2.sh $
# $Id: checkSnmp2.sh 1392 2012-02-03 20:23:30Z dlhaines $

set -x

OID=$1;

###########
# snmp command that works from fiesta
# snmpget -c public -v 1 probe.dsc.umich.edu system.sysDescr.0
# result of command
#SNMPv2-MIB::sysDescr.0 = STRING: Linux probe.dsc.umich.edu 2.6.32-220.4.1.el6.x86_64 #1 SMP Thu Jan 19 14:50:54 EST 2012 x86_64
###########

#snmpcommand="snmpget -c public -v 1 probe.dsc.umich.edu system.sysDescr.0";
#OID=".1.3.6.1.4.1.789.1.2.1.1";
snmpcommand="snmpget -c public -v 1 probe.dsc.umich.edu $OID";
snmpregex="SNMPv2-MIB::sysDescr.0 = STRING: .*.dsc.umich.edu";

snmpresult=$($snmpcommand);

echo "check basic snmp access";
echo "snmpcommand: [$snmpcommand]";
echo "snmpregex: [$snmpregex]";
echo "snmpresult: [$snmpresult]";

if [[ "$snmpresult" =~ $snmpregex ]] ; 
then
    echo "snmp request worked";
    exit 0;
else
    echo "snmp request failed";
    exit 1;
fi

#end
