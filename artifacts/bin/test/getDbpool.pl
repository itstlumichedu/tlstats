#!/usr/bin/perl -w
# Script to get active / idle db pool values from ctools instances
# Requires host name as argument.

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/getDbpool.pl $
# $Id: getDbpool.pl 1379 2012-02-03 16:08:49Z dlhaines $

use strict;

my $DEBUG_FILE_NAME="getDbpool";
my $TRACE=0;
my $FAILED_TIME=2;
my $TIMEOUT=$FAILED_TIME;

my $DEBUG_DIR="../debug";

# Reading from @_ and returning string makes testing much more natural.
sub main {

  my $HOST = shift;

  if ($TRACE) {
    my $OUTPUTFILE="$DEBUG_DIR/DEBUG-${DEBUG_FILE_NAME}.TXT";
    open (MYFILE, ">>$OUTPUTFILE");
  }

  my $DATE=`date`;
  chomp($DATE);

  print MYFILE "$DATE: HOST: [$HOST]\n" if ($TRACE);
  my @m= `wget --timeout=$TIMEOUT -q --no-check-certificate https://${HOST}/ctools-jsp/dbpool.jsp -O -`;
  chomp(@m);

  if ($TRACE) {
    print MYFILE "m:",join("|",@m),"\n";
    print MYFILE "m[0]:",$m[0],"\n";
    print MYFILE "m[1]:",$m[1],,"\n";
  }

  ### Input data format from jsp is:
  #Active|0|Idle|50|

  my ($active,$idle) = $m[1] =~ m/Active\|(\d+)\|Idle\|(\d+)|/sm;
  # Put in U (undefined) unless got a reasonable answer.
  $active = "U" unless(defined($active));
  $idle = "U" unless(defined($idle));

  print MYFILE "active:$active idle:$idle\n" if ($TRACE);

  # return the data.
  return "active:$active idle:$idle\n";
}

###########

### Run the script unless it is called from a test file.
print main(@ARGV) unless ( $0 =~ m|.*\.t| );

# Need to return 1 if called from a test file.
1;
#end
