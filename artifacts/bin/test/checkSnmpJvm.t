#!/usr/bin/perl -w
#
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/checkSnmpJvm.t $
# $Id: checkSnmpJvm.t 1416 2012-02-06 01:18:11Z dlhaines $
#
# Copyright (C) 2000-2007 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats about Sun JVM
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

use strict;
use Test::More qw(no_plan);
use FindBin;
use RRDs;
use SNMP_util;

my $homeDir = ${FindBin::Bin}."/../..";

require "$homeDir/bin/monitor.pl";

local $| = 1;

my($config);
$config->{'STEP'} = 60;
$config->{'DEBUG'} = 0;
$config->{'program'} = 'jvmThreadMon';
$config->{'version'} = "0.01";
#$config->{'homeDir'} = $ENV{'HOME'};
#$config->{'homeDir'} = ${FindBin::Bin}."/../..";
$config->{'homeDir'} = $homeDir;
#print "FB: $FindBin::Bin\n";
#print "homedir: ",$config->{'homeDir'},"\n";
$config->{'libDir'} = $config->{'homeDir'} . "/lib";
$config->{'mibDir'} = $config->{'libDir'} . "/mibs";
$config->{'dataDir'} = $config->{'homeDir'} . "/data/jvm";
$config->{'timeout'} = 15;
#if (! defined $config->{'logfacility'}){
#  $config->{'logfacility'} = 'user';
#}

my $monitorFile = "$homeDir/etc/jvm.mon";

# check to allow processing only certain servers.
#my $guardname = "CTIR_ctdev";
my $guardname = "ctstats";

#printConfig($config);
printHash($config,"jvmmon");

#openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

use lib qw($config->{'libDir'});

#use strict;
#local $| = 1;


print "What about cache file: [$SNMP_util::CacheFile]\n";
snmpLoad_OID_Cache($SNMP_util::CacheFile);
#$SNMP_Session::suppress_warnings = 2;
$SNMP_Session::suppress_warnings = 0;
$SNMP_util::Debug=1;


#my @files = returnFileAsArray($config->{'mibDir'});

opendir(DIR, $config->{'mibDir'}) || die "can't opendir $config->{'mibDir'}: $!";
my(@files) =readdir(DIR);
closedir DIR;


ok(scalar(@files) > 0,"found mib files");


#print "read mib files: ",join("|",@files),"\n";

for my $mib (@files){
    chomp($mib);
    print "checking mib: [$mib]\n";
    $mib = $config->{'mibDir'} . "/" . $mib;
    next if ($mib =~ /^\./); #skip dot files
    next unless ($mib =~ /\.mib/); #skip any files that don't have mib in their name
    print "adding mib: [$mib]\n";
    my($rv) = &snmpQueue_MIB_File($mib);
#    if (! $rv && $config->{'DEBUG'}){
#	print STDERR "* ERROR: Could not load $mib: $rv\n";
#    }
}

print "have read mib files\n";


my($hostname);
my(@threadcols) = (
             "jvmThreadInstName",
#            "jvmThreadInstState",
             "jvmThreadInstCpuTimeNs",
             "jvmThreadInstWaitTimeMs",
             "jvmThreadInstWaitCount",
             "jvmThreadInstBlockTimeMs",
             "jvmThreadInstBlockCount",
#             "jvmThreadInstLockName",
		   );


my(@mempoolcols) = (
		    "jvmMemPoolName",
		    "jvmMemPoolInitSize",
		    "jvmMemPoolUsed",
		    "jvmMemPoolCommitted",
#		    "jvmMemPoolMaxSize",
#		    "jvmMemPoolPeakUsed",
#		    "jvmMemPoolPeakCommitted",
#		    "jvmMemPoolPeakMaxSize",
		    "jvmMemPoolCollectUsed",
		    "jvmMemPoolCollectCommitted",
#		    "jvmMemPoolCollectMaxSize",
	     );


sub threadupdate {
  print "update thread: ",join("|",@_),"\n";
#    update('thread', @_);
}

# sub update {
#     my($type) = shift(@_);
#     my($first) = shift(@_);
#     my($RRD);
#     my($STEP) = $config->{'STEP'};

#     if ($type eq "mempool" || $type eq "memmgr"){
# 	my($name) = shift(@_);
# 	$name =~ s/\s+/_/g;
# 	$RRD = $config->{'dataDir'} . "/" . $type . "/" . $hostname . "." . $first . "_" . $name . ".rrd";
#     } elsif ($type eq "jvmagg"){
# 	$RRD = $config->{'dataDir'} . "/" . $type . "/" . $hostname . ".rrd";
#     } elsif ($type eq "thread"){
# 	my($name) = shift(@_);
# 	$name =~ s/\s+/_/g;
# 	my($threaddir) = $config->{'dataDir'} . "/" . $type . "/" . $hostname;
# 	if (! -d "${threaddir}"){
# 	    if(! mkdir "${threaddir}", 0750){
# 		notify('crit', "Could not create dir ${threaddir} ($!), aborting!");
# 		exit;
# 	    }
# 	}
# 	$RRD = $threaddir . "/" . $name . "." . $first . ".rrd";
#     }
#     my(@vals) = @_;

#     my($t) = time;
#     $t = int($t);

#     for (my $i=0;$i<=$#vals;$i++){
# 	if (! $vals[$i] || $vals[$i] eq ''){
# 	    $vals[$i] = 0;
# 	}
# 	my $timeval = $vals[$i];
# 	chomp($timeval);
# 	if ($timeval =~ /\d*\:\d+\:\d+/){
# 	    $timeval =~ s/(\d*)\:(\d+)\:(\d+)//;
# 	    my ($hrs, $mins, $secs) = ($1, $2, $3);
# 	    $vals[$i] = $hrs * 3600 + $mins * 60 + $secs;
# 	}
#     }

#     #print STDERR $RRD . "|" . $t . "|" . join('|', @vals) . "\n";
#     #return;

#     # if (! -e $RRD){
#     # 	my($START) = time - (2 * $STEP);
  
#     # 	notify('info', "Creating $RRD with step ${STEP} starting at $START");
#     # 	my($v, $msg) = RRD_create($RRD, $START, $STEP, $type);
#     # 	if ($v){
#     # 	    notify('err', "couldn't create $RRD because $msg");
#     #     return;
#     #   } 
#     # } 

#     # my($rv, $errmsg) = updateRRD($RRD, $t, @vals);
#     # if ($rv){
#     # 	notify('err', "error updating $RRD : ${errmsg}");
#     # }
# }

# sub RRD_create {
#   print "RRD_create exit\n";
#   exit 1;
#     my($RRD, $START, $interval, $type) = @_;
#     my(@dses);
#     my(@rras) = (
# 		 "RRA:AVERAGE:0.5:1:3000",
# 		 "RRA:MAX:0.5:1:3000",
# 		 "RRA:AVERAGE:0.5:5:3000",
# 		 "RRA:MAX:0.5:5:3000",
# 		 "RRA:AVERAGE:0.5:10:5000",
# 		 "RRA:MAX:0.5:10:5000",
# 		 "RRA:AVERAGE:0.5:1440:732",
# 		 "RRA:MAX:0.5:1440:732"
#  		 );

#     if ($type eq "mempool"){
# 	@dses = (
# 		    "DS:mempoolisz:GAUGE:1200:U:U",
# 		    "DS:mempoolused:GAUGE:1200:U:U",
# 		    "DS:mempoolcomm:GAUGE:1200:U:U",
# 		    "DS:mempoolcollu:GAUGE:1200:U:U",
# 		    "DS:mempoolcollcomm:GAUGE:1200:U:U",
# 	     );
#     } elsif ($type eq "memmgr"){
# 	@dses = (
# 		 "DS:memmgrgc:COUNTER:1200:U:U",
# 		 "DS:memmgrgctms:COUNTER:1200:U:U",
# 		 );
#     } elsif ($type eq "thread"){
# 	@dses = (
#              "DS:cputimens:COUNTER:1200:U:U",
#              "DS:waittimems:COUNTER:1200:U:U",
#              "DS:waitcount:COUNTER:1200:U:U",
#              "DS:blocktimems:COUNTER:1200:U:U",
#              "DS:blockcount:COUNTER:1200:U:U",
# 		 );
#     } elsif ($type eq "jvmagg"){
# 	@dses = (
# 		 "DS:classesloaded:ABSOLUTE:1200:U:U",
# 		 "DS:classesunloaded:ABSOLUTE:1200:U:U",
# 		 "DS:memheapinit:GAUGE:1200:U:U",
# 		 "DS:memheapused:GAUGE:1200:U:U",
# 		 "DS:memheapcomm:GAUGE:1200:U:U",
# 		 "DS:memnonheapinit:GAUGE:1200:U:U",
# 		 "DS:memnonheapused:GAUGE:1200:U:U",
# 		 "DS:memnonheapcomm:GAUGE:1200:U:U",
# 		 "DS:threadcount:GAUGE:1200:U:U",
# 		 "DS:threaddaemoncount:GAUGE:1200:U:U",
# 		 "DS:threadtotstarted:COUNTER:1200:U:U",
# 		 "DS:rtuptimems:COUNTER:1200:U:U",
# 		 "DS:jitcompilertms:COUNTER:1200:U:U",
# 		 );
#     } else {
# 	notify('ERR', "could not create RRD of type ${type}");
# 	return(1, "do not recognize type ${type}");
#     }


#     RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses, @rras);

#     if (my $error = RRDs::error()) {
# 	return(1, "Cannot create $RRD: $error");
#     } else {
# 	return(0, "$RRD");
#     }
# }

my($targets);

my @urlLines = returnFileAsArray($monitorFile);

#while(<>){
foreach (@urlLines){
    chomp;
    print "config input: [$_]\n";
#    next if (/(^$)|(^#)/);
    next if (/^($|#)/);
    my($hostname, $community, $ip) = split(/\|/);
    print "h: [$hostname] c: [$community] ip: [$ip]\n";
    $targets->{$hostname}->{'community'} = $community;
    $targets->{$hostname}->{'ip'} = $ip;
}

print "read targets file\n";
#printConfig($targets);
printHash($targets,"jvm targets");

#exit 1;

my($last) = time;
my($next) = $last;

while(1){
    my($startTime) = time;
    for my $h (keys %{$targets}){
	$hostname = $h;
	my($community, $ip) = ($targets->{$hostname}->{'community'}, $targets->{$hostname}->{'ip'});
	my($agent) = $community . "\@" . $ip;
	
	print "agent: [$agent] community: [$community] ip: [$ip]\n";
#	  print "cols: ",join("#",@threadcols),"\n";
	  print "cols: ",join("#",@mempoolcols),"\n";
#	if ($hostname =~ /^ctools\./){
	# set guard name for ctir

	print "hostname: [$hostname] guardname: [$guardname]\n";
#	if ($hostname =~ /^CTIR\./){
	if ($hostname =~ /^$guardname\./){
	  print "+++ calling snmp\n";
#	    notify('debug', "starting thread collection for ${hostname}");
#my(@mempoolcols) = (
#	    my ($nrows) = &snmpmaptable($agent, \&threadupdate, { 'use_getbulk' => 0 }, @threadcols);
	    my ($nrows) = &snmpmaptable($agent, \&threadupdate, { 'use_getbulk' => 0 }, @mempoolcols);
	  print "nrows: $nrows\n";
	    # if (! $nrows){
	    # 	notify('err', "error retrieving threadoids from ${hostname}");	
	    # }
	    # notify('debug', "ending thread collection for ${hostname}");
	}
    }
    my($nextRun) = $startTime + $config->{'STEP'};
    my($sleep) = ($nextRun - time); 
    if ($sleep < 0){
	$sleep = abs($sleep);
	notify('err', "collector ran for $sleep seconds too long, sleeping for 1 second", 400);
	$sleep = 1;
    }
    notify('debug', "DEBUG: Sleeping till $nextRun ($sleep seconds)", 200);
      print "sleeping: $sleep\n";
    sleep $sleep;
}

