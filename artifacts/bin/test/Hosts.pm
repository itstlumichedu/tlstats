# 
# Module to store configuration parameters for the 
# Cacti data source scripts.

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/Hosts.pm $
# $Id: Hosts.pm 1417 2012-02-06 01:18:21Z dlhaines $

## Calling scripts should have:
# our $testGoodHost;
# our $testBadHost;

our $testGoodHost = "probe.dsc.umich.edu";
our $testBadHost = "ArmyBootsSmell";

# Required to return 1 as this is a module.
1;
#end
