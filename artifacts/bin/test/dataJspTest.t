#!/usr/bin/perl -w
# test access to the ora database pages.
# 
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/dataJspTest.t $
# $Id: dataJspTest.t 1678 2012-05-04 15:45:46Z dlhaines $
#
# Copyright (C) 2000-2007 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats from a Oracle server (via a JSP)
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

use strict;
use Test::More qw(no_plan);
use FindBin;
my $homeDir = ${FindBin::Bin}."/../..";
require "$homeDir/bin/monitor.pl";

my $trace = 0;

my $authnsource = "";
my $config = {};

$config->{'DEBUG'} = 0;
$config->{'program'} = 'oraMonitor';
$config->{'version'} = "0.01";
$config->{'homeDir'} = $homeDir;
$config->{'dataDir'} = $config->{'homeDir'}."/data/db";

#openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

my $monitorFile = "$homeDir/etc/ora.test.monitor";
print "homeDir: [$homeDir] monitorFile: [$monitorFile]\n" if ($trace);
my @urlLines = returnFileAsArray($monitorFile);

my ($n,$h);	 # hold instance and host from url for later printing.
my ($f);	 # hold name of the properties file if it exists.
my ($events,$dbstats) = (0,0);	# mark what kind of query this is.

my @list;

my $instances = {};

foreach (@urlLines) {
  chomp;
  next if (/(^#)|(^$)/);
  my($server, $notify, $user, $pass, $statUrl, $dbname) = split(/\|/);
  my $key = "$dbname.$user.$server";
  $instances->{$key}->{'user'} = $user;
  $instances->{$key}->{'pass'} = $pass;
  $instances->{$key}->{'statUrl'} = $statUrl;
  $instances->{$key}->{'dbname'} = $dbname;
  $instances->{$key}->{'notify'} = $notify;
}

#printHash($instances,"ora instances");
# fail if there is nothing to do.
if (scalar(keys(%{$instances})) == 0) {
  fail("no test targets specified in monitor file.");
  exit 1;
}

for my $tc (sort keys %{$instances}) {
  my($statUrl) = $instances->{$tc}->{'statUrl'};
  my($dbname) = $instances->{$tc}->{'dbname'};
  my($user) = $instances->{$tc}->{'user'};
  my($passwd) = $instances->{$tc}->{'pass'};
  my($timeout) = 60;
  my($notify) = $instances->{$tc}->{'notify'};

  if ($user ne "" && $passwd ne "") {
  } else {
    fail("input line must have user and password");
  }

  print "key: [$tc]\n" if ($trace);

  # extract some useful info from url
  ($n,$h,$f) = (undef,undef,undef);
  #  ($n) = ($statUrl =~ m|n=([^&]+)|);
  #  ($h) = ($statUrl =~ m|h=([^&]+)|);
  #  ($f) = ($statUrl =~ m|f=([^&]+)|);

  $n = ($statUrl =~ m|n=([^&]+)| ? $1 : undef);
  $h = ($statUrl =~ m|h=([^&]+)| ? $1 : undef);
  $f = ($statUrl =~ m|f=([^&]+)| ? $1 : undef);
  $events = ($statUrl =~ m|events.jsp| ? 1 : 0);
  $dbstats = ($statUrl =~ m|dbstats.jsp| ? 1 : 0);

  print "n:[$n] h:[$h] f:[$f]\n" if ($trace);

  my $queryType = "";
  $queryType .= ($events ? "events " : " ");
  $queryType .= ($dbstats ? "dbstats " : " ");

  #  print "jsp: events: [$events] dbstats: [$dbstats] f: [$f]\n";
  print "jsp: queryType: [$queryType]\n" if ($trace);

  print "statUrl: [$statUrl]\n" if ($trace);
  my($nrval, $ncode, $nstatus, $ns, $ntcp, $nf, $nz, $ncontent) = check_httpd($statUrl, $user, $passwd, $timeout);
  $authnsource = "";
  $authnsource .= " n: [$n]" if ($n);
  $authnsource .= " h: [$h]" if ($h);
  $authnsource .= " f: [$f]" if ($f);

  print "authnsource: [$authnsource]\n" if ($trace);

  #  is($ncode,200,"find requested page for n: [$n] h: [$h]");

  print "ncontent: [$ncontent]\n" if ($trace);
  is($ncode,200,"find requested $queryType page for source $authnsource");
  if ($ncode != 200) {
    fail("page not found");
    next;
  }

  my @result;
  (@list) = split(/\n/, $ncontent);
  chomp(@list);
  ok(scalar(@list)>0,"list has contents");

  verifyAbsent(qr/java.*Exception/i);

  if ($dbstats) {
    verifyPresent(qr/sesstat/i);
    verifyPresent(qr/dbmetric/i);
  }

  if ($events) {
    verifyPresent(qr/sakai_site_tool/i);
  }

}
sub verifyAbsent {
  my $regex = shift;
  my @result = grep { $_ =~ $regex } @list;
  #    ok(scalar(@result) == 0,"did not find $regex: in n: [$n] h: [$h]");
  ok(scalar(@result) == 0,"should not find $regex: using $authnsource");
}

sub verifyPresent {
  my $regex = shift;
  my @result = grep { $_ =~ $regex } @list;
  #    ok(scalar(@result) > 0,"found $regex: in n: [$n] h: [$h]");
  ok(scalar(@result) > 0,"found $regex: using $authnsource");
  if (scalar(@result) == 0) {
    print "verifyPresent failed list: ",join("\n",@list),"\n";
  }
}
