#!/usr/bin/perl -w
#
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/userCount.t $
# $Id: userCount.t 1575 2012-03-21 16:39:44Z dlhaines $
#
# Copyright (C) 2000-2007 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve the number of users and event aggregates 
# using ctools servers
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

## Cut down version of userCount to test data access

#require '/ctstats/bin/monitor.pl';

## read in the usercount mon file
## parse out url
## get the result
## verify sanity.

use strict;
use Test::More qw(no_plan);

use FindBin;

my $trace = 0;

my @list;
my $authnsource;

my $homeDir = ${FindBin::Bin}."/../..";

require "$homeDir/bin/monitor.pl";

my($http_timeout) = 240;

## Read in the file of web pages to check
my $monitorFile = "$homeDir/etc/userCount.test.mon";

my @urlLines = returnFileAsArray($monitorFile);

# pull out the seperate urls
my(%targets);
foreach (@urlLines) {
  chomp;
  next if (/(^#)|(^\s*$)/);
  my($url, $cluster) = split(/\|/);
  $targets{$url} = $cluster;
}

# Good for debugging
#printHash(\%targets,"userCountTest targets");

# fail if there is nothing to do.
if (scalar(keys(%targets)) == 0) {
  fail("no test targets specified in monitor file.");
  exit 1;
}


my($n,$h,$f);

# sanity check results of each url.
for my $url (sort keys %targets) {

  # extract some useful info from url
  ($n,$h,$f) = (undef,undef,undef);

  $n = ($url =~ m|n=([^&]+)| ? $1 : undef);
  $h = ($url =~ m|h=([^&]+)| ? $1 : undef);
  $f = ($url =~ m|f=([^&]+)| ? $1 : undef);

  $authnsource = "";
  $authnsource .= " n: [$n]" if ($n);
  $authnsource .= " h: [$h]" if ($h);
  $authnsource .= " f: [$f]" if ($f);

  print "authnsource: [$authnsource]\n" if ($trace);

#  $events = ($statUrl =~ m|events.jsp| ? 1 : 0);
#  $dbstats = ($statUrl =~ m|dbstats.jsp| ? 1 : 0);

  my($rval, $code, $status, $s, $tcp, $f, $z, $content, $md5) = check_httpd($url,"","",$http_timeout);

  is($code,200,"find requested page for ".$targets{$url});

  $content =~ s/\<body\>/\n/g;
  $content =~ s/\<br\>/|\n/g;

  print "content: [$content]\n" if ($trace);

  (@list) = split(/\n/, $content);

  # make sure these don't appear in the output

  verifyAbsent(qr/SQLSyntaxErrorException/i);
  verifyAbsent(qr/ERROR\|/i);

  # make sure these strings appear in the output
  my @result;



  verifyPresent(qr/runtime/i);
  # @result = grep { $_ =~ /runtime/i } @list;
  # ok(scalar(@result) > 0,"find runtime message");

  verifyPresent(qr/sakai_site_tool/i);
  # @result = grep { $_ =~ /sakai_site_tool/i } @list;
  # ok(scalar(@result) > 0,"find sakai_site_tool message");

}

sub verifyAbsent {
  my $regex = shift;
  my @result = grep { $_ =~ $regex } @list;
  #    ok(scalar(@result) == 0,"did not find $regex: in n: [$n] h: [$h]");
  ok(scalar(@result) == 0,"should not find $regex: using $authnsource");
}

sub verifyPresent {
  my $regex = shift;
  my @result = grep { $_ =~ $regex } @list;
  #    ok(scalar(@result) > 0,"found $regex: in n: [$n] h: [$h]");
  ok(scalar(@result) > 0,"found $regex: using $authnsource");
  if (scalar(@result) == 0) {
    print "verifyPresent failed list: ",join("\n",@list),"\n";
  }
}
