#!/usr/bin/perl -w

## Collect statistics on sakai db pools and eue
## NOTE UMIAC not implemented
## eue being added

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/sakaiStats.t $
# $Id: sakaiStats.t 2146 2015-06-04 16:50:55Z dlhaines $

use Test::More qw(no_plan);
use FindBin;
use RRDs;
my $homeDir = ${FindBin::Bin}."/../..";
require "$homeDir/bin/monitor.pl";

local $| = 1;

## Read in the file of web pages to check
my $monitorFile = "$homeDir/etc/sakaiStats.test.mon";

my @urlLines = returnFileAsArray($monitorFile);

our($config);
$config->{'STEP'} = 60;
$config->{'DEBUG'} = 0;
$config->{'program'} = 'sakaiStats';
$config->{'version'} = "0.02";
#$config->{'homeDir'} = $ENV{'HOME'};
$config->{'homeDir'} = $homeDir;
$config->{'libDir'} = $config->{'homeDir'} . "/lib";
$config->{'mibDir'} = $config->{'libDir'} . "/mibs";
$config->{'dataDir'} = $config->{'homeDir'} . "/data/sakai";
$config->{'timeout'} = 1;

use lib qw($config->{'libDir'});

my(%urls) = (
	     "dbpool" => "/ctools-jsp/dbpool.jsp",
	     "eue"    => "/access/content/public/ok.txt",
	     # Umiac status is not implemented
	     #	     "umiac" => ":8888/getStats",
	    );

# Get the list of servers to check.
my(%farm);
foreach (@urlLines) {
  next if (/(^$|^#)/);
  chomp;
  my($host, $addr) = split(/\|/);
  $farm{$host} = $addr;
}

# useful for debugging
# printHash(\%farm,"sakaiStats Farm");

# fail if there is nothing to do.
if (scalar(keys(%farm)) == 0) {
  fail("no test targets specified in monitor file.");
  exit 1;
}

# for each hosts
for my $host (keys %farm) {
  my($addr) = $farm{$host};

  # for all the types of urls specified in urls hash
  # check out the results.
  for my $type (keys %urls) {

    my($url) = "https://" . $addr . $urls{$type};
    # if ($type eq "umiac") {
    #   $url = "http://" . $addr . $urls{$type};
    # }
    my($rval, $code, $status, $s, $tcp, $f, $z, $content, $md5) = check_httpd($url,"","",$config->{'timeout'});
    is($code,200,"url get successful");

    if (defined($rval) && $rval > 0 && $code == 200) {
	#print "content: [$content]\n";
      $content =~ s/\<body\>/\n/g;
      $content =~ s/\<br\>/|\n/g;
      my(@list) = split(/\n/, $content);
      my($i) = 1;
      my($collectionTime) = int($f);

      for my $line (@list) {
	chomp($line);
	#	print "line: [$line]\n" if ($TRACE);
	next if ($line =~ /^\s*$/);
	next if ($line =~ /this\ script\ runtime/);
	if ($type eq "dbpool") {
	  my($id1, $val1, $id2, $val2) = split(/\|/, $line);
	  like($id1,qr/Active/i,"dbpool active label correct");
	  like($val1,qr/^\d+$/,"dbpool returned integer value");
	  like($id2,qr/Idle/i,"dbpool active idle label correct");
	  like($val2,qr/^\d+$/,"dbpool returned integer value");
	} elsif ($type eq "eue") {
		like($line,qr/ok-/i,"returned ok file");
		print "eue: line: [$line]\n" if ($config->{'DEBUG'}); 
	} elsif ($type eq "umiac") {
	  fail("Umiac checking not implemented");
	}
      }
    }
  }
}

