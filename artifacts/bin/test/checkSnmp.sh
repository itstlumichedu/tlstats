#!/bin/bash

# Run simple snmpget commands on hosts so can see if snmp works with that server.

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/checkSnmp.sh $
# $Id: checkSnmp.sh 1906 2013-03-20 15:51:28Z dlhaines $

### TTD
# print out any failing response.
# make basic snmp check function callable by itself and still print newline

#set -x
# force error when see an unset variable.
set -u

TRACE=1;

function checkBasicSnmp {
    HOST=$1;

    echo "################ check basic snmp access for [$HOST]";

    snmpcommand="snmpget -c public -v 1 $HOST system.sysDescr.0";
#    snmpregex="SNMPv2-MIB::sysDescr.0 = STRING: .*.umich.edu";
    snmpregex="SNMPv2-MIB::sysDescr.0 = STRING: (.*.umich.edu|Data ONTAP)";

    snmpresult=$($snmpcommand);

    if [[ "$snmpresult" =~ $snmpregex ]] ; 
    then
#	echo "++++++++ basic snmp request worked";
	echo -n "   basic snmp worked";
    else
#	echo "-------- basic snmp request failed";
	echo "   -------- BASIC SNMP FAILED";
	if [ 0 -ne $TRACE ]; then
	    echo "snmpcommand: [$snmpcommand]";
	    echo "snmpregex: [$snmpregex]";
	    echo "snmpresult: [$snmpresult]";
	fi;
    fi
}

function checkBasicJvmSnmp {
    HOST=$1;

    snmpcommand="snmpget -d -c public -v 1 $HOST  .1.3.6.1.4.1.42.2.145.3.163.1.1.4.1.0";
    snmpregex="SNMPv2-SMI::enterprises.42.2.145.3.163.1.1.4.1.0 = STRING: \"[0-9]+@$HOST\"";

#    echo "################ check basic snmp JVM access for [$HOST]";

    snmpresult=$($snmpcommand);

    if [[ "$snmpresult" =~ $snmpregex ]] ; 
    then
	echo -n "   jvm snmp request worked";
    else
	echo -n "   ------- JVM SNMP REQUEST FAILED";
	if [ 0 -ne $TRACE ]; then
	    echo " ";
	    echo "snmpcommand: [$snmpcommand]";
	    echo "snmpregex: [$snmpregex]";
	    echo "snmpresult: [$snmpresult]";
	fi;
    fi
}

function checkBasicJvmSnmpSmall {
    HOST=$1;

    snmpcommand="snmpget -d -c public -v 1 $HOST  .1.3.6.1.4.1.42.2.145.3.163.1.1.4.1.0";
    snmpregex="SNMPv2-SMI::enterprises.42.2.145.3.163.1.1.4.1.0 = STRING: \"[0-9]+@$HOST\"";

    echo "################ check basic snmp JVM access for [$HOST]";

    snmpresult=$($snmpcommand);

    if [[ "$snmpresult" =~ $snmpregex ]] ; 
    then
#	echo "++++++ jvm snmp request worked";
	echo -n "jvm works ";
    else
#	echo "------- jvm snmp request failed";
	echo " JVM FAILS ";
	if [ 0 -ne $TRACE ]; then
	    echo " ";
	    echo "snmpcommand: [$snmpcommand]";
	    echo "snmpregex: [$snmpregex]";
	    echo "snmpresult: [$snmpresult]";
	fi;
    fi

}


function checkSnmpForHost {
    HOST=$1
    checkBasicSnmp $HOST
    echo " ";
    checkBasicJvmSnmp $HOST
    echo " ";
}

function printHeader {
    echo "running" $(basename $0) "on" $(hostname) "at:" $(date);
}

function printReason {
    echo -e "\n**** checking $@";
}

printHeader


### Ctdev located at ASB
echo " ";
echo "check CTIR ctdev servers";
checkSnmpForHost prefect.dsc.umich.edu;
checkSnmpForHost probe.dsc.umich.edu;

### CTIR ctqa  at ASB
echo " ";
checkSnmpForHost scorpio.dsc.umich.edu
checkSnmpForHost special.dsc.umich.edu
checkSnmpForHost squire.dsc.umich.edu

### ctir load runners servers
checkBasicSnmp neon.dsc.umich.edu
checkBasicSnmp newport.dsc.umich.edu

### CTIR ctint located at ASB
echo " ";
checkSnmpForHost taurus.dsc.umich.edu
checkSnmpForHost tempo.dsc.umich.edu

### CTIR ctload at MACC
echo " ";
echo "check CTIR ctload servers";
checkSnmpForHost sail.dsc.umich.edu
checkSnmpForHost senator.dsc.umich.edu;
checkSnmpForHost silverado.dsc.umich.edu;
checkSnmpForHost sonic.dsc.umich.edu;
checkSnmpForHost spark.dsc.umich.edu;
checkSnmpForHost spectrum.dsc.umich.edu;
checkSnmpForHost standard.dsc.umich.edu;
checkSnmpForHost suburban.dsc.umich.edu;


### CTIR prod at MACC
echo " ";
echo "check CTIR prod servers";
checkSnmpForHost tahoe.dsc.umich.edu;
checkSnmpForHost tavera.dsc.umich.edu;
checkSnmpForHost tigra.dsc.umich.edu;
checkSnmpForHost tornado.dsc.umich.edu;
checkSnmpForHost tosca.dsc.umich.edu;
checkSnmpForHost townsman.dsc.umich.edu;
checkSnmpForHost tracker.dsc.umich.edu;
checkSnmpForHost traverse.dsc.umich.edu;
# added for 2.9 release (maybe)
checkSnmpForHost talon.dsc.umich.edu;
checkSnmpForHost tucker.dsc.umich.edu;
checkSnmpForHost templar.dsc.umich.edu;
checkSnmpForHost totem.dsc.umich.edu;

### CTIR db servers
echo " ";
echo "check CTIR db ctload servers";
checkBasicSnmp yeoman.dsc.umich.edu;
echo " ";
checkBasicSnmp galaxy.dsc.umich.edu;
echo " ";
# prod db server
checkBasicSnmp malibu.dsc.umich.edu;
echo " ";
checkBasicSnmp courser.dsc.umich.edu;

### CTIR ctstats servers
echo " ";
echo " ";
checkSnmpForHost fiesta.dsc.umich.edu
checkSnmpForHost journey.dsc.umich.edu
#checkBasicSnmp fiesta.dsc.umich.edu
#checkBasicSnmp journey.dsc.umich.edu


#### UMIAC

printReason "CTIR umiac ctint"

checkBasicSnmp zephyr.dsc.umich.edu
checkBasicSnmp zodiac.dsc.umich.edu

printReason "CTIR umiac ctqa"

checkBasicSnmp bluebonnet.dsc.umich.edu
checkBasicSnmp brilliant.dsc.umich.edu

printReason "CTIR umaic ctdev"

checkBasicSnmp ranchero.dsc.umich.edu
checkBasicSnmp roadster.dsc.umich.edu

printReason "CTIR umaic ctload"

checkBasicSnmp satellite.dsc.umich.edu
checkBasicSnmp shadow.dsc.umich.edu
checkBasicSnmp sierra.dsc.umich.edu
checkBasicSnmp spirit.dsc.umich.edu
checkBasicSnmp sprinter.dsc.umich.edu
checkBasicSnmp stealth.dsc.umich.edu

printReason "CTIR umaic prod"

checkBasicSnmp caliber.dsc.umich.edu
checkBasicSnmp caravan.dsc.umich.edu
checkBasicSnmp cavalier.dsc.umich.edu
checkBasicSnmp challenger.dsc.umich.edu
checkBasicSnmp charger.dsc.umich.edu
checkBasicSnmp colt.dsc.umich.edu


### sample output for snmpstatus.  The errors aren't a problem since server
### is not a router.

# snmpstatus -v 1 -c public probe.dsc.umich.edu 
# Error in packet.
# Reason: (noSuchName) There is no such variable name in this MIB.
# Failed object: IP-MIB::ipOutRequests.0

# Error in packet.
# Reason: (noSuchName) There is no such variable name in this MIB.
# Failed object: IP-MIB::ipInReceives.0

# [UDP: [141.211.149.133]:161->[0.0.0.0]]=>[Linux probe.dsc.umich.edu 2.6.32-220.4.1.el6.x86_64 #1 SMP Thu Jan 19 14:50:54 EST 2012 x86_64] Up: 4 days, 23:46:17.60
# Interfaces: 0, Recv/Trans packets: 0/0 | IP: 0/0

###########
# snmp command that works from fiesta
# snmpget -c public -v 1 probe.dsc.umich.edu system.sysDescr.0
# result of command
#SNMPv2-MIB::sysDescr.0 = STRING: Linux probe.dsc.umich.edu 2.6.32-220.4.1.el6.x86_64 #1 SMP Thu Jan 19 14:50:54 EST 2012 x86_64
###########


#end
