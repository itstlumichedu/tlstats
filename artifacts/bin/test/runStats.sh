#!/bin/bash
# run all the working stats collection scripts
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/runStats.sh $
# $Id: runStats.sh 1464 2012-02-14 19:33:02Z dlhaines $

#BIN="$(pwd)/..";
#ETC="$BIN/../etc";

BIN="/usr/local/ctools/ctstats-wush/ctstats-drraw/bin";
ETC="/usr/local/ctools/ctstats-wush/ctstats-drraw/etc";

echo -n "update started at: ";
date;
echo "BIN: $BIN ETC: $ETC";

#ls $BIN;
#ls $ETC;

# by default collected every 1 min
$BIN/jvmMon.pl $ETC/ctir.jvm.mon;

# by default collected every 5 min
$BIN/sakaiStats.pl $ETC/sakaiStats.mon
$BIN/userCount.pl $ETC/userCount.mon
$BIN/tcMonitor.pl $ETC/tomcat.monitor
$BIN/oraMonitor.pl $ETC/ora.monitor

echo "NOT WORKING AS YET: "
echo " core, netapp, ACE (instead of netscalar)";

### Not working as yet
# # collect T1/T2 core stats every 1 minute
# */1 * * * * ${HOME}/bin/coreMon.pl ${HOME}/etc/core.mon

# # collect netscaler and netapp stats every 5 minutes
# */5 * * * * ${HOME}/bin/netscaler.pl ${HOME}/etc/netscaler.monitor
# */5 * * * * ${HOME}/bin/checkNetapp.pl ${HOME}/etc/netapp.monitor

#end
