#!/usr/bin/perl -w

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/checkSnmp.t $
# $Id: checkSnmp.t 1380 2012-02-03 16:32:44Z dlhaines $

# Placeholder for simple snmp test.

#################
# treat string as file handle: http://www.perlmonks.org/?node_id=32740
# my $testStringAsFile = "HOWDY\nHOWDY AGAIN";
# Example of how to read from a string using a filehandle.
# sub readFromString {
#   my $fh;
#   open($fh,"<",\$testStringAsFile) or die("can not open string as file handle $!");
#   my @lines = <$fh>;
#   print join("|",@lines),"\n";
# }
# readFromString();
#################

use strict;
use Test::More qw(no_plan);

fail("$0 tests not implemented");

# # Need more valid urls
# my $f1 = "tempo.dsc.umich.edu|EMAIL|USER|PW|https://tempo.dsc.umich.edu/manager/freeMem.jsp|HOWDY";

# my $debug = 0;

# our($config);
# our($tomcats);

# # Get the file to test.
# BEGIN {
#   require_ok('checkHttpd.pl');
# }

# sub debugHttpd {
#   my ($rval, $r_code, $r_status_line, $start, $tcp_end, $finish, $contentsize, $content, $md5) = @_;
#   if ($debug) {
#     print join("|","rval: ",$rval,"r_code",$r_code,"status_line",$r_status_line,"start",$start,"tcp_end",$tcp_end,
# 	       "finish",$finish,"contentsize",$contentsize,"content",$content,"md5",$md5),"\n";
    
#     my $time = $finish - $start;
#     print "total time: ",$time," tcp time: ",$tcp_end - $start,"\n";
#   }
# }

# sub checkGoodFreeMemSite {

#   $debug = 0;
#   my $timeout = 1;
#   my $freeMemSite = "https://tempo.dsc.umich.edu/manager/freeMem.jsp";

#   my ($rval, $r_code, $r_status_line, $start, $tcp_end, $finish, $contentsize, $content, $md5) 
#     = check_httpd($freeMemSite, "", "", $timeout);

#   is($rval,1,"return code from good site");

#   my($freeMem,$totalMem) = split(/\|/,$content);

#   debugHttpd($rval, $r_code, $r_status_line, $start, $tcp_end, $finish, $contentsize, $content, $md5)  if ($debug);

#   cmp_ok($freeMem,'>',1000000,"check plausable free memory");
#   cmp_ok($totalMem,'>=',$freeMem,"check plausable total memory");
# }


# sub checkBadHost {

#   $debug = 0;
#   my $timeout = 1;
#   my $freeMemSite = "https://tempoXXX.dsc.umich.edu/manager/freeMem.jsp";

#   my ($rval, $r_code, $r_status_line, $start, $tcp_end, $finish, $contentsize, $content, $md5) 
#     = check_httpd($freeMemSite, "", "", $timeout);

#   debugHttpd($rval, $r_code, $r_status_line, $start, $tcp_end, $finish, $contentsize, $content, $md5)  if ($debug);

#   isnt($rval,1,"return code from bad site");
#   is($r_code,500,"bad host");

# }

# sub checkBadPage {

#   $debug = 0;
#   my $timeout = 1;
#   my $freeMemSite = "https://tempo.dsc.umich.edu/manager/freeMemXXX.jsp";

#   my ($rval, $r_code, $r_status_line, $start, $tcp_end, $finish, $contentsize, $content, $md5) 
#     = check_httpd($freeMemSite, "", "", $timeout);

#   debugHttpd($rval, $r_code, $r_status_line, $start, $tcp_end, $finish, $contentsize, $content, $md5)  if ($debug);

#   isnt($rval,1,"return code from bad page");
#   is($r_code,404,"missing page");

# }


# # don't know how to get a site that must time out, so this isn't checked.
# # sub checkTimeout {

# #   $debug = 1;
# #   my $timeout = 0.000001;
# #   my $freeMemSite = "https://tempo.dsc.umich.edu/manager/freeMem.jsp";

# #   my ($rval, $r_code, $r_status_line, $start, $tcp_end, $finish, $contentsize, $content, $md5) 
# #     = check_httpd($freeMemSite, "", "", $timeout);

# #   is($rval,1,"return code from timeout");

# #   my($freeMem,$totalMem) = split(/\|/,$content);

# #   debugHttpd($rval, $r_code, $r_status_line, $start, $tcp_end, $finish, $contentsize, $content, $md5)  if ($debug);

# # }


# # sub printHash {
# #   my $href = shift;
# #   foreach(sort(keys(%$href))) {
# #     print "k: [$_] => v: [",$href->{$_},"]\n";
# #   }
# # }

# # # Ensure that can read the input argument using data from Hosts module.
# # #like(main($testBadHost),qr/A:1 B:2 arg1:ArmyBootsSmell$/i,"model returns supplied argument");

# # # Ensure that can default a value when not passed an argument.
# # #like(main(),qr/A:1 B:2 arg1:U$/i,"model returns default argument");

# checkGoodFreeMemSite();
# checkBadHost();
# checkBadPage();

# # Don't know how to get a request that must time out.
# # checkTimeout();

#end
