#!/usr/bin/perl
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/checkAllDbpoolAccess.t $
# $Id: checkAllDbpoolAccess.t 1590 2012-03-28 17:38:53Z dlhaines $

# Sanity test for the getDbpool cacti data script

### Should be revised to use monitor file not Hosts.pm.

use strict;
use Test::More qw(no_plan);

use FindBin;
use lib "$FindBin::Bin";

my $homeDir = ${FindBin::Bin}."/../..";
require "$homeDir/bin/monitor.pl";

BEGIN {
	require_ok('getDbpool.pl');
}

## These are defined in the Hosts module.
#our $testGoodHost;
#our $testBadHost;

# check all the dbpool hosts listed in sakaiStats.mon

my $monitorFile = "$homeDir/etc/sakaiStats.test.mon";

my @urlLines = returnFileAsArray($monitorFile);

foreach(@urlLines) {
    next if (/(^$|^#)/);
    chomp;
    my($host, $addr) = split(/\|/);
#    print "host: [$host] addr: [$addr]\n";
    like(main($addr),qr/active:\d+\s+idle:\d+/,"result from instance: $host name $addr is as expected");
}

exit 1;

#like(main($testBadHost),qr/active:U\s+idle:U/i,"result from bad host is plausible");
#like(main($testGoodHost),qr/active:\d+\s+idle:\d+/,"result from good host is as expected");

#isnt (length($mail1),0,"mail1 var has text");
#isnt (length($mail2),0,"mail2 var has text");

## check output xml
#is(returnXmlCheck("kungpao","Fri Dec  2 17:51:38 EST 2011",3),
#   '<event start="Fri Dec  2 17:51:38 EST 2011" title="kungpao">  kungpao check: 3 </event>',
#   "nagios check xml");

#is(returnXmlRestart("kungpao","Fri Dec  2 17:51:38 EST 2011"),
#   '<event start="Fri Dec  2 17:51:38 EST 2011" title="kungpao">  kungpao restart </event>',
#   ,"nagios restart xml");

# is_deeply($mock{$r},["XXX"],"check response value");


#end
