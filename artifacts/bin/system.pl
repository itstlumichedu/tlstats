#!/usr/local/bin/perl -w

use strict;
local $| = 1;

use Socket;
use SNMP_Session;
use BER;
$main::DEBUG = 0;
$main::SNMPDEBUG =0;

#
# A restricted snmpget.
#

sub snmpget {
    my($host,$community,@vars) = @_;
  my(@enoid, $var,$response, $bindings, $binding, $value, $inoid,$outoid,
     $upoid,$oid,@retvals);
    foreach $var (@vars) {
	if ($var =~ /^([a-z]+)/i) {
	    my $oid = $snmpget::OIDS{$1};
	    if ($oid) {
		$var =~ s/$1/$oid/;
	    } else {
        die "Unknown SNMP var $var\n"
	}
	}
	print "SNMPGET OID: $var\n" if $main::DEBUG >5;
	push @enoid,  encode_oid((split /\./, $var));
    }
    srand();
    my $session;
    eval('$session = SNMP_Session->open($host,$community,161)');
    if ($@) {
	warn "SNMPGET Problem for $community\@$host:\n*** $@\n";
	return (-1,-1);
    }

    if ($session->get_request_response(@enoid)) {
	$response = $session->pdu_buffer;
	($bindings) = $session->decode_get_response ($response);
	$session->close ();
	while ($bindings) {
	    ($binding,$bindings) = decode_sequence ($bindings);
	    ($oid,$value) = decode_by_template ($binding, "%O%@");
	    my $tempo = pretty_print($value);
	    $tempo=~s/\t/ /g;
	    $tempo=~s/\n/ /g;
	    $tempo=~s/^\s+//;
	    $tempo=~s/\s+$//;
	    push @retvals,  $tempo;
	}
	return (@retvals);
    } else {
	return (-1,-1);
    }
}

%snmpget::OIDS =
  ('sysDescr' => '1.3.6.1.2.1.1.1.0',
   'sysContact' => '1.3.6.1.2.1.1.4.0',
   'sysName' => '1.3.6.1.2.1.1.5.0',
   'sysLocation' => '1.3.6.1.2.1.1.6.0',
   'sysUptime' => '1.3.6.1.2.1.1.3.0',
   'sysObjectID' => '1.3.6.1.2.1.1.2.0',
   'ifNumber' =>  '1.3.6.1.2.1.2.1.0',
   );

my $router = $ARGV[0];
my $community = $ARGV[1] || "public";

my(
$descr,
$objid,
$location,
$contact,
$name,
$uptime,
$ifNumber
) =
  &snmpget($router,
           $community,
           'sysDescr',
           'sysObjectID',
           'sysLocation',
           'sysContact',
           'sysName',
           'sysUptime',
           'ifNumber');


my $addr = (gethostbyname($router))[4];
my $ip = join(".",unpack('C4', $addr));
$descr =~ s/^M/\n/g;

print "$router ($ip)\nsysName: $name\nsysDescr: $descr\nsysLocation: $location\n";

print "sysContact: $contact\nsysObjectID: $objid\nsysUptime:$uptime\nifNumber: $ifNumber\n";
