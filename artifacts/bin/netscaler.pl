#!/usr/local/bin/perl -w
#
# Copyright (C) 2000-2006 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats about vservers from a Netscaler
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

use strict;
local $| = 1;

use RRDs;

use FindBin;
my $homeDir = ${FindBin::Bin}."/..";

require "$homeDir/bin/monitor.pl";

#require '/ctstats/bin/monitor.pl';

my($config);
$config->{'STEP'} = 300;
$config->{'DEBUG'} = 0;
$config->{'program'} = 'netscaler';
$config->{'version'} = "0.01";

#$config->{'homeDir'} = $ENV{'HOME'};
$config->{'homeDir'} = $homeDir;

$config->{'libDir'} = $config->{'homeDir'} . "/lib";
$config->{'mibDir'} = $config->{'libDir'} . "/mibs";

#$config->{'dataDir'} = $config->{'homeDir'} . "/data/jvm";
#print "**** HACKING DATA DIR in $0 ****\n";
#$config->{'dataDir'} = $config->{'homeDir'} . "/data/netscaler";
$config->{'dataDir'} = "$sharedDataDir/netscaler";

$config->{'timeout'} = 15;
if (! defined $config->{'logfacility'}){
  $config->{'logfacility'} = 'user';
}

openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

use lib qw($config->{'libDir'});

#use strict;
#local $| = 1;

use SNMP_util;
## Override the default cache file ("OID_cache.txt") with "cache_test.txt"
#$SNMP_util::CacheFile = "cache_test.txt";
$SNMP_Session::suppress_warnings = 2;
snmpLoad_OID_Cache($SNMP_util::CacheFile);

opendir(DIR, $config->{'mibDir'}) || die "can't opendir $config->{'mibDir'}: $!";
my(@files) =readdir(DIR);
closedir DIR;

for my $mib (@files){
    chomp($mib);
    $mib = $config->{'mibDir'} . "/" . $mib;
    next if ($mib =~ /^\./); #skip dot files
    next unless ($mib =~ /\.mib/); #skip any files that don't have mib in their name
    my($rv) = &snmpQueue_MIB_File($mib);
#   my($rv) = &snmpMIB_to_OID($mib);
    if (! $rv && $config->{'DEBUG'}){
	print STDERR "* ERROR: Could not load $mib: $rv\n";
    }
}

my($netscaler, $community);

my(@vservercols) = (
	     "vsvrName", 
	     "vsvrIpAddress",
	     "vsvrTotalRequests",
	     "vsvrTotalResponses",
	     "vsvrTotalRequestBytes",
	     "vsvrTotalResponseBytes",
	     "vsvrTotalPktsRecvd",
	     "vsvrTotalPktsSent",
	     "vsvrTotalSynsRecvd",
  	     "lbvsvrAvgSvrTTFB",
	     "lbvsvrActiveConn",
	     );

sub vserverupdate {
    update('vserver', @_);
}


my(@vsrvrsuppcols) = (
	     "vsvrName", 
	     "vsvrIpAddress",
#	     "vsvrAvgTransactionTime",
 	    "lbvsvrAvgSvrTTFB",
	     "vsvrMaxReqPerConn",
	     "vsvrCurClntConnections",
	     "vsvrCurSrvrConnections",
#	     "vsvrSurgeCount",
	     "vsvrCurServicesUnKnown",
    );
sub vsvrsuppupdate {
    update('vsvrsupp', @_);
}

my(@svcols) = (
	       "svcServiceName",
	       "svcIpAddress",
	       "svcMaxReqPerConn",
	       "svcAvgTransactionTime",
	       "svcEstablishedConn",
	       "svcActiveConn",
	       "svcSurgeCount",
	       "svcTotalRequests",
	       "svcTotalResponses",
	       "svcTotalRequestBytes",
	       "svcTotalResponseBytes",
	       "svcTotalPktsRecvd",
	       "svcTotalPktsSent",
	       "svcTotalSynsRecvd",
	       );

sub svupdate {
    update('service', @_);
}

my(@svcgrpcols) = (
		   "svcGrpMemberName",
		   "svcGrpMemberPrimaryIPAddress",
		   "svcGrpMemberMaxReqPerConn",
		   "svcGrpMemberAvgTransactionTime",
		   "svcGrpMemberEstablishedConn",
		   "svcGrpMemberActiveConn",
		   "svcGrpMemberSurgeCount",
		   "svcGrpMemberTotalRequests",
		   "svcGrpMemberTotalResponses",
		   "svcGrpMemberTotalRequestBytes",
		   "svcGrpMemberTotalResponseBytes",
		   "svcGrpMemberAvgSvrTTFB",
		   "svcGrpMemberTotalPktsRecvd",
		   "svcGrpMemberTotalPktsSent",
		   "svcGrpMemberTotalSynsRecvd",
		   );

sub svcgrpupdate {
    update('svcgrp', @_);
}

my(@syscols) = (
		"resCpuUsage",
		"resMemUsage",
	       );

my(@oldsyscols) = (
"tcpTotClientConnOpened",
"tcpTotServerConnOpened",
"tcpErrSentRst",
"tcpReuseHit",
"tcpErrStrayPkt",
"tcpTotZombieCltConnFlushed",
"tcpTotZombieSvrConnFlushed",
"tcpCurPhysicalServers",
#"tcpErrCookiePktSeqReject",
#"tcpErrCookiePktSigReject",
"nsCPUusage",
"tcpErrSynGiveUp",
"httpTotRequests",
"httpTotGets",
"httpTotPosts",
"httpTotResponses",
"httpTotClenResponses",
"httpTotChunkedResponses",
"httpErrNoreuseMultipart",
"httpErrIncompleteHeaders",
"httpErrIncompleteRequests",
"httpErrIncompleteResponses",
"httpTot11Responses",
"httpErrServerBusy",
#"totudpsessions",
    );

sub sysupdate {
    update('sys', @_);
}

sub oldsysupdate {
    update('netscaler', @_);
}

my(@conncols) = (
		 "tcpCurClientConn",
		 "tcpCurServerConn",
		 "tcpCurClientConnEstablished",
		 "tcpCurServerConnEstablished",
		 "tcpCurPhysicalServers",
		 );

sub connupdate {
    update('connections', @_);
}

my(@oldaclcols) = (
"aclName",
"aclPriority",
"aclHits",
		);


my(@aclcols) = (
"aclName",
"aclPriority",
"aclperHits",
		);


sub aclupdate {
    update('acl', @_);
}

my(@sslcols) = (
"sslTotTransactions",
"sslTotSSLv2Transactions",
"sslTotSSLv3Transactions",
"sslTotTLSv1Transactions",
"sslTotSessionHits",
"sslTotSSLv2Sessions",
"sslTotSSLv3Sessions",
"sslTotTLSv1Sessions",
"sslTotExpiredSessions",
"sslTotNewSessions",
"sslTotSessionHits",
"sslTotSessionMiss",
#"sslTotNULLAuthorizations"
		);
sub sslupdate {
    update('ssl', @_);
}


sub update {
    my($type) = shift(@_);
    my($first) = shift(@_);

    print "$0: update: ",join("|",@_),"\n";

    my($name, $ipaddr);
    if ($type eq "vserver" || $type eq "vsvrsupp" || $type eq "service" || $type eq "svcgrp"){
	$name = shift(@_);
	$ipaddr = shift(@_);
    } elsif ($type eq "acl"){
	$name = shift(@_);
	my $priority = shift(@_);
	$name = $name . "_" . $priority;
    } elsif ($type eq "netscaler" || $type eq "connections" || $type eq "ssl" || $type eq "sys"){
	$name = $netscaler;
    }

    if (! -d "$config->{'dataDir'}/${type}/${netscaler}"){
	if(! mkdir "$config->{'dataDir'}/${type}/${netscaler}", 0750){
	    notify('crit', "Could not create $config->{'dataDir'}/${type}/${netscaler} ($!), skipping");
	    next;
	}
    }

    my($RRD) = $config->{'dataDir'} . "/" . $type . "/" . $netscaler . "/" . $name . ".rrd";

    my(@vals) = @_;

    for (my $i=0;$i<=$#vals;$i++){
	if ($vals[$i]){
	    my $timeval = $vals[$i];
	    chomp($timeval);
	    if ($timeval =~ /\d*\:\d+\:\d+/){
		$timeval =~ s/(\d*)\:(\d+)\:(\d+)//;
		my ($hrs, $mins, $secs) = ($1, $2, $3);
		$vals[$i] = $hrs * 3600 + $mins * 60 + $secs;
	    }
	} else {
	    notify('debug', "value ${i} for set ${RRD} is undef");
	    $vals[$i] = 0;
	}
    }

    if (! -e $RRD){
      my($START) = time - $config->{'STEP'};

      if (! ensureRRDDirectory($RRD)) {
	notify('crit', "Could not create ${RRD} ($!), skipping");
      }
  
      notify('info', "Creating $RRD with step $config->{'STEP'} starting at $START");
      my($v, $msg) = RRD_create($RRD, $START, $config->{'STEP'}, $type);
      if ($v){
        notify('err', "couldn't create $RRD because $msg");
        return;
      } 
    } 

    my($t) = time;
    $t = int($t);
    if ($config->{'DEBUG'}){
	print STDERR $RRD . "|" . $t . "|" . join('|', @vals) . "\n";
    }
    my($rv, $errmsg) = updateRRD($RRD, $t, @vals);
    if ($rv){
	notify('err', "error updating $RRD : ${errmsg}");
    }
}

sub RRD_create {
    my($RRD, $START, $interval, $type) = @_;
    my(@dses);

    if ($type eq "vserver"){
	@dses = (
		 "DS:requests:COUNTER:600:U:U",
		 "DS:responses:COUNTER:600:U:U",
		 "DS:requestbytes:COUNTER:600:U:U",
		 "DS:responsebytes:COUNTER:600:U:U",
		 "DS:pktsrecvd:COUNTER:600:U:U",
		 "DS:pktssent:COUNTER:600:U:U",
		 "DS:synsrecvd:COUNTER:600:U:U",
		 "DS:ttfb:GAUGE:600:U:U",
		 "DS:conns:GAUGE:600:U:U",
		 );
    } elsif ($type eq "service"){
	@dses = (
		 "DS:svcmaxreqperconn:GAUGE:600:U:U",
		  "DS:svcavgtranstime:GAUGE:600:U:U",
		  "DS:svcestablishedconn:GAUGE:600:U:U",
		  "DS:svcactiveconn:GAUGE:600:U:U",
		  "DS:svcsurgecount:GAUGE:600:U:U",
		  "DS:svctotalreqsts:COUNTER:600:U:U",
		  "DS:svctotresp:COUNTER:600:U:U",
		  "DS:svctotreqbyte:COUNTER:600:U:U",
		  "DS:svctotrespbyte:COUNTER:600:U:U",
		  "DS:svctotpktsrecvd:COUNTER:600:U:U",
		  "DS:svctotpktssent:COUNTER:600:U:U",
		  "DS:svctotsynsrecvd:COUNTER:600:U:U",
		 );
    } elsif ($type eq "svcgrp"){
	@dses = (
		 "DS:svcmaxreqperconn:GAUGE:600:U:U",
		 "DS:svcavgtranstime:GAUGE:600:U:U",
		 "DS:svcestablishedconn:GAUGE:600:U:U",
		 "DS:svcactiveconn:GAUGE:600:U:U",
		 "DS:svcsurgecount:GAUGE:600:U:U",
		 "DS:svctotalreqsts:COUNTER:600:U:U",
		 "DS:svctotresp:COUNTER:600:U:U",
		 "DS:svctotreqbyte:COUNTER:600:U:U",
		 "DS:svctotrespbyte:COUNTER:600:U:U",
		 "DS:svctotpktsrecvd:COUNTER:600:U:U",
		 "DS:svctotpktssent:COUNTER:600:U:U",
		 "DS:svctotsynsrecvd:COUNTER:600:U:U",
		 "DS:svcavgttfb:GAUGE:600:U:U",
		 );
    } elsif ($type eq "sys"){
	@dses = (
		 "DS:rescpuusage:GAUGE:600:U:U",
		 "DS:resmemusage:GAUGE:600:U:U",
		 );
    } elsif ($type eq "netscaler"){
	@dses = (
		 "DS:totclntconns:COUNTER:600:U:U",
		 "DS:totsrvrconns:COUNTER:600:U:U",
		 "DS:clntconnRefused:COUNTER:600:U:U",
		 "DS:reusehit:COUNTER:600:U:U",
		 "DS:reusemiss:COUNTER:600:U:U",
		 "DS:totclntdontreuse:COUNTER:600:U:U",
		 "DS:totsrvrdontreuse:COUNTER:600:U:U",
		 "DS:totphysicalsrvrs:COUNTER:600:U:U",
		 "DS:cpuusage:GAUGE:600:U:U",
		 "DS:unacksyn:COUNTER:600:U:U",
		 "DS:totreq:COUNTER:600:U:U",
		 "DS:totgets:COUNTER:600:U:U",
		 "DS:totposts:COUNTER:600:U:U",
		 "DS:totresp:COUNTER:600:U:U",
		 "DS:totcontentlenresp:COUNTER:600:U:U",
		 "DS:totchunkedresp:COUNTER:600:U:U",
		 "DS:totmultipartresp:COUNTER:600:U:U",
		 "DS:totincmplthdr:COUNTER:600:U:U",
		 "DS:totincmpltreq:COUNTER:600:U:U",
		 "DS:totincmpltresp:COUNTER:600:U:U",
		 "DS:totpipelinedreq:COUNTER:600:U:U",
		 "DS:srvrbusyerrs:COUNTER:600:U:U",
#		 "DS:totudpsess:COUNTER:600:U:U",
		 );
    } elsif ($type eq "ssl"){
	@dses = (
		 "DS:tottxn:COUNTER:600:U:U",
		 "DS:totv2txn:COUNTER:600:U:U",
		 "DS:totv3txn:COUNTER:600:U:U",
		 "DS:tottlsv1txn:COUNTER:600:U:U",
		 "DS:totsess:COUNTER:600:U:U",
		 "DS:totv2sess:COUNTER:600:U:U",
		 "DS:totv3sess:COUNTER:600:U:U",
		 "DS:tottlsv1sess:COUNTER:600:U:U",
		 "DS:totexpsess:COUNTER:600:U:U",
		 "DS:totnewsess:COUNTER:600:U:U",
		 "DS:totsesshits:COUNTER:600:U:U",
		 "DS:totsessmiss:COUNTER:600:U:U",
		 );
    } elsif ($type eq "acl"){
	@dses = (
		 "DS:hits:COUNTER:600:U:U",
		 );
    } elsif ($type eq "vsvrsupp"){
	@dses = (
	       "DS:avgtranstime:GAUGE:600:U:U",
	       "DS:maxreqperconn:GAUGE:600:U:U",
	       "DS:curclntestdconn:GAUGE:600:U:U",
	       "DS:cursrvrestdconn:GAUGE:600:U:U",
	       "DS:surgecount:GAUGE:600:U:U",
	       );
    } elsif ($type eq "connections"){
	@dses = (
		 "DS:curclntconn:GAUGE:600:U:U",
		 "DS:cursrvrconn:GAUGE:600:U:U",
		 "DS:curclntestconn:GAUGE:600:U:U",
		 "DS:cursrvrestconn:GAUGE:600:U:U",
		 "DS:curphyssrvr:GAUGE:600:U:U",
		 );
    } else {
	notify('ERR', "could not create RRD of type ${type}");
	return(1, "do not recognize type ${type}");
    }


    RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses,
		  "RRA:AVERAGE:0.5:1:43800",
		  "RRA:MAX:0.5:1:43800",
		  "RRA:AVERAGE:0.5:6:600",
		  "RRA:MAX:0.5:6:600",
		  "RRA:AVERAGE:0.5:24:600",
		  "RRA:MAX:0.5:24:600",
		  "RRA:AVERAGE:0.5:288:732",
		  "RRA:MAX:0.5:288:732"
		  );

    if (my $error = RRDs::error()) {
	return(1, "Cannot create $RRD: $error");
    } else {
	return(0, "$RRD");
    }
}

while(<>){
    next if (/(^#)|(^$)/);
	       chomp;
	       ($netscaler, $community) = split(/\|/);
	       my($agent) = $community . "\@" . $netscaler;

	       my(%sets);
	       $sets{"vservercols"}{"cols"} = \@vservercols;
	       $sets{"vservercols"}{"func"} = \&vserverupdate;
	       $sets{"svcols"}{"cols"} = \@svcols;
	       $sets{"svcols"}{"func"} = \&svupdate;
	       $sets{"svcgrp"}{"cols"} = \@svcgrpcols;
	       $sets{"svcgrp"}{"func"} = \&svcgrpupdate;
	       $sets{"syscols"}{"cols"} = \@syscols;
	       $sets{"syscols"}{"func"} = \&sysupdate;
	       $sets{"conncols"}{"cols"} = \@conncols;
	       $sets{"conncols"}{"func"} = \&connupdate;
	       $sets{"sslcols"}{"cols"} = \@sslcols;
	       $sets{"sslcols"}{"func"} = \&sslupdate;
	       $sets{"aclcols"}{"cols"} = \@aclcols;
	       $sets{"aclcols"}{"func"} = \&aclupdate;

	       #aclHits is obseleted in favor of aclperHits
	       #if ($netscaler eq "141.211.140.29" || $netscaler eq "141.211.253.193"){
	       #   $sets{"aclcols"}{"cols"} = \@newaclcols;
	       #'}

	       #"vsrvrsuppcols" => \@vsrvrsuppcols,

	       for my $set (keys %sets){
		   my($rv) = &snmpmaptable($agent, $sets{$set}{"func"}, { 'use_getbulk' => 0 }, @{$sets{$set}{"cols"}});
		   if (!defined($rv) || $rv < 0){
		       notify('err', "getting ${set} from ${netscaler}");
		   }
	       }

#    my($nrows) = &snmpmaptable($agent, \&vserverupdate, { 'use_getbulk' => 0 }, @vservercols);
#    #my($vsupprows) = &snmpmaptable($agent, \&vsvrsuppupdate, { 'use_getbulk' => 0 }, @vsrvrsuppcols);
#    my($srows) = &snmpmaptable($agent, \&svupdate, { 'use_getbulk' => 0 }, @svcols);
#    my($sysrows) = &snmpmaptable($agent, \&sysupdate, { 'use_getbulk' => 0 }, @syscols);
#    my($connrows) = &snmpmaptable($agent, \&connupdate, { 'use_getbulk' => 0 }, @conncols);
#    my($sslrows) = &snmpmaptable($agent, \&sslupdate, { 'use_getbulk' => 0 }, @sslcols);

}

