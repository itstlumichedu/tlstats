<%-- Test to see if can read file from absolute file path --%>

<%@page language="java" import="java.util.*,java.sql.*,oracle.jdbc.driver.*" %>
<%@page import="java.io.InputStream" %>
<%@page import="java.io.*" %>
<%@page import="java.util.Properties" %>

<%-- TTD
- use proper error message when file not found.
- make into method in header or dbheader
- verify that uses the file from the URL if supplied
--%>
 
<%

	// default properties file for testing.
	//String f = "/Users/dlhaines/tmp/wush/ctools/ctstats/dataapp/test-resource/testAccounts.properties";
	// The default will be in the webapp for testing.  The real path to the properties file should be passed on 
	// query URL.
	String f = "WEB-INF/testAccounts.properties";
	String defaultFileName = f;
			
	String user;
	String password;

	String traceParameter = request.getParameter("t");

	int trace = 0;
	if (traceParameter != null && !"0".equals(traceParameter)) {
		trace = 1;
	}
		
	// make the properties file name to use
	String fileNameFromRequest = request.getParameter("f");

	String fileName = defaultFileName;
	if (fileNameFromRequest != null && !fileNameFromRequest.equals("")){
		fileName = fileNameFromRequest;
	}
	else {
		fileName = config.getServletContext().getRealPath(f);
		System.out.println("default path: "+fileName);
	}


	if (trace != 0) { 
%>
		properties file name: <%=fileName%> !!
<%
	}
%>

<%-- get the properties file --%>

<%

	// get the properties file

	Properties properties = new Properties();

	try {
		properties.load(new FileInputStream(fileName));
	} catch (IOException e) {
		System.out.println("fileCheck.jsp: properties file error: "+e);
		%>
		<p/>
		properties file error: <%= e.toString() %>
		<%
		response.setStatus(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		return;
	}
	
	// read the properties
	if (trace > 0) {
		System.out.println("user: "+properties.getProperty("user"));
	}
	
	user = properties.getProperty("user");
	password = properties.getProperty("password");
	//System.out.println("password: "+properties.getProperty("password"));
	if (trace > 0) {
%>
	<p/>
    user: <%=user%>
    <p/>
    password: NEVER YOU MIND
	<p/>
	<%
	}
	 %>
<%
	//user: <%=properties.getProperty("user")
	// password: <%= properties.getProperty("password")
%>
	
<%-- END --%>
