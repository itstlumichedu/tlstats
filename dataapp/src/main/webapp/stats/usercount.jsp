<%@ page language="java" import="java.util.*,java.sql.*,oracle.jdbc.driver.*" 
%><%@ include file="common/getProperties.jsp" 
%><%@ include file="common/header.jsp"
%><%@ include file="common/dbheader.jsp"%><%

	String query;

	query = "select session_server, count(distinct(session_user)) as num_users, count(session_id) as num_sessions from sakai_session "
		+"where session_start >= SYSDATE - 1 and session_start = session_end group by session_server order by session_server";

	//query = "select * from SAKAI_SESSION";
		
		if (trace > 0) {
			%> <p/>query: <%=query%> <p/>
			<%
		}
		
        Statement stmt = conn.createStatement();
        ResultSet rset = stmt.executeQuery(query);

        while (rset.next()){
              String server = (rset.getString(1)).replaceAll("-.*", "");
	      String users = rset.getString(2);
	      String sessions = rset.getString(3);
%><%=server%>|<%=users%>|<%=sessions%><p/>
<%
        }
	rset.close();
        stmt.close();

	conn.close();

%><%@ include file="common/dbfooter.jsp"
%><%@ include file="common/footer.jsp"%>
