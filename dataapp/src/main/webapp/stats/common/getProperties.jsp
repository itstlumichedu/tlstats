<%-- Read in a properties file. --%>
<%-- Can supply from the query (in f parameter) as full path, as relative path in servlet or use default from servlet. 
     Also allows setting the t (trace) parameter. 
     Provides the global variable properties for the properties file.
     Provides method setPropertyDefault to allow setting property from URL defaulting to value in properties file. 
     --%>
<%@page language="java" import="java.util.*,java.sql.*,oracle.jdbc.driver.*" 
%><%@page import="java.io.InputStream" 
%><%@page import="java.io.*" 
%><%@page import="java.util.Properties" 
%><%-- TTD
- use proper error message when file not found.
- make into method in header or dbheader
- verify that uses the file from the URL if supplied
--%>
 <%-- Method to assign default value from properties file (as a string) 
 --%><%!
 String setPropertyDefault(Properties properties, String name, String value) {
	 //System.out.println("p: "+properties+" n: "+name+" v: "+value);
	 if (value != null && !"".equals(value)) {
		 return value;
	 }
	 return properties.getProperty(name);
 }
 %><%

Properties properties = new Properties();

	// default properties file for testing.
	//String f = "/Users/dlhaines/tmp/wush/ctools/ctstats/dataapp/test-resource/testAccounts.properties";
	// The default will be in the webapp for testing.  The real path to the properties file should be passed on 
	// query URL.
	String f = "WEB-INF/testAccounts.properties";
	String defaultFileName = f;
		
	String traceParameter = request.getParameter("t");

	int trace = 0;
	if (traceParameter != null && !"0".equals(traceParameter)) {
		trace = 1;
	}
		
	// make the properties file name to use
	String fileNameFromRequest = request.getParameter("f");

	String fileName = defaultFileName;
	if (fileNameFromRequest != null && !fileNameFromRequest.equals("")){
		fileName = fileNameFromRequest;
	}
	else {
		fileName = config.getServletContext().getRealPath(f);
		//System.out.println("default path: "+fileName);
	}
	
	if (trace != 0) { 
%>
		properties file name: <%=fileName%> !!
<%
	}
%><%

	// get the properties file

	try {
		properties.load(new FileInputStream(fileName));
	} catch (IOException e) {
		System.out.println("fileCheck.jsp: properties file error: "+e);
		%>
		<p/>
		properties file error: <%= e.toString() %>
		<%
		response.setStatus(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		return;
	}
%><%-- END --%>
