<%@ page language="java" import="java.util.*,java.sql.*,oracle.jdbc.driver.*" 
%><%
    DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
	int defTimeout = DriverManager.getLoginTimeout();
	int localdbConnTimeout = 10;
	DriverManager.setLoginTimeout(localdbConnTimeout);
	
	String dbhost = request.getParameter("h"); 
	String dbport = request.getParameter("p");
	String dbname = request.getParameter("n");
	String dbuser = request.getParameter("u");
	String dbpwd = request.getParameter("w");
	
	dbhost = setPropertyDefault(properties,"dbhost",request.getParameter("h"));
	dbport = setPropertyDefault(properties,"dbport",request.getParameter("p"));
	dbname = setPropertyDefault(properties,"dbname",request.getParameter("n"));
	dbuser = setPropertyDefault(properties,"dbuser",request.getParameter("u"));
	dbpwd = setPropertyDefault(properties,"dbpwd",request.getParameter("w"));

	// Existing url format did not work ?
	//String jdbcurl = "jdbc:oracle:thin:" + dbuser + "/" + dbpwd + "@" + dbhost + ":" + dbport + ":" + dbname;
	//String jdbcurl = "jdbc:oracle:thin:" + dbuser + "/" + dbpwd + "@//" + dbhost + ":" + dbport + "/" + dbname;
	String jdbcurl = "jdbc:oracle:thin:@" + dbhost + ":" + dbport + ":" + dbname;
	
	if (trace > 0) {
		System.out.println("jdbcurl: "+jdbcurl);
		%> 
		jdbcurl=<%=jdbcurl %>
		<%
	}

        Connection conn = null;
	try {
//		conn = DriverManager.getConnection(jdbcurl);
		conn = DriverManager.getConnection(jdbcurl,dbuser,dbpwd);
	} catch (Exception dbe){
		response.setStatus(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
%>Default timeout: <%=defTimeout%> reset to <%=localdbConnTimeout%> seconds
<%=dbe.toString()%><%		
	}
	
	if (conn == null) {
		response.setStatus(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		return;
	}
%>
