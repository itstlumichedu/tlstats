<%@ include file="../common/getProperties.jsp" 
%><%@ include file="../common/header.jsp"
%><%@ include file="../common/dbheader.jsp"%><%
String pwd = request.getParameter("pwd");
if (pwd != null && pwd.equals("XXXXXXXXX")){
        response.setContentType("text/plain");

	String q;
	String machine = request.getParameter("m");


	if (machine != null && !machine.equals("")){
		String queries[] = {
			"select s.sid, s.username, s.blocking_session, s.status, s.sql_id, a.sql_text as \"sqltext\" from v$session s left outer join v$sqlarea a on s.sql_id = a.sql_id where s.machine = ? order by s.sid",
			"SELECT sw.sid, s.username, s.blocking_session, sw.event, sw.state, sw.seconds_in_wait, sw.wait_class FROM v$session_wait sw, v$session s WHERE sw.sid=s.sid and s.machine= ? order by sw.SECONDS_IN_WAIT, sw.sid, sw.state"
	};

		for (int j=0;j<queries.length;j++){
			PreparedStatement pstmt = conn.prepareStatement(queries[j]);
			pstmt.setString(1, machine);

			ResultSet rset = pstmt.executeQuery();
			ResultSetMetaData rsmd = rset.getMetaData();
		     	int numberOfColumns = rsmd.getColumnCount();
			for (int k=1;k<numberOfColumns;k++){
				String colname = rsmd.getColumnLabel(k);
%><%=colname%>|<%
			}
%>
<%
		        while (rset.next()){
				for ( int i = 1; i <= numberOfColumns; i++){ 
		                	String val = rset.getString(i);
%><%=val%>|<%
				}
%>
<%
		        }
			rset.close();
		        pstmt.close();
%>

<%
		}
	}
} else {
	response.setStatus(javax.servlet.http.HttpServletResponse.SC_FORBIDDEN);
}
%><%@ include file="../common/dbfooter.jsp"
%><%@ include file="../common/footer.jsp"%>
