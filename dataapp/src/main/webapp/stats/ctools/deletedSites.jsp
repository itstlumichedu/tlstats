<%@ page language="java" import="java.util.*,java.sql.*,oracle.jdbc.driver.*,javax.naming.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"
%><sql:setDataSource dataSource="jdbc:oracle:thin:ctools_readonly/XXXXXXXX@ctoolsdb.vip.itd.umich.edu:1521:CTOOLS" 
/><c:catch var="ex"><c:choose>	<c:when test="${param.p == 'cruft'}"><sql:query var="delfiles">
select
 d.site_id,
 d.delete_date,
 i.title,
 replace(c.resource_id, '|', '.'),
 c.file_path
from
 content_resource c, sakai_site_deleted d left outer join sakai_site i on d.site_id = i.site_id 
where
 d.site_id is not null
 and d.site_id != '(null)'
 and d.delete_date < (current_date - interval '365' day(3))
 and d.delete_date > (current_date - interval '465' day(3)) 
 and c.context = d.site_id
 and c.file_path not like '/fs%/2009/%'
 and rownum <= 4000
order by
 d.site_id,
 d.delete_date,
 c.resource_id,
 c.file_path
</sql:query><c:if test="${not empty delfiles.rows}"><% response.setContentType("text/plain"); out.flush(); %>
<c:forEach var="af" items="${delfiles.rows}">
<c:out value="${af.site_id}"/>|<c:out value="${af.delete_date}"/>|<c:out value="${af.title}"/>|<c:out value="${af.resource_id}"/>|<c:out value="${af.file_path}"/></c:forEach>
</c:if></c:when><c:otherwise>
		<% response.setStatus(javax.servlet.http.HttpServletResponse.SC_FORBIDDEN); %>
	</c:otherwise></c:choose></c:catch><c:if test="${not empty ex}">
	<h3>ERROR operation not completed</h3>
	<c:out value="${ex.message}" />	<% response.setStatus(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR); %></c:if>
