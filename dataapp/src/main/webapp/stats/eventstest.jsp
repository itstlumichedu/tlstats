<%@ include file="common/getProperties.jsp" 
%><%@ include file="common/header.jsp"
%><%@ include file="common/dbheader.jsp"%><%
        response.setContentType("text/plain");

	String q1 = "select 'users', session_server, 'placeholder', count(distinct(session_user)) as num_users, count(session_id) as num_sessions from sakai_session where session_start >= SYSDATE - 1 and session_start = session_end group by session_server";

	String q2 = "select 'event', case when event is null then 'null.event' else event end as event, 'placeholder', count(*) as count, 0 from sakai_event where event_date > (sysdate + interval '03:55' hour to minute) and event_date < (sysdate + interval '4' hour(1)) group by event";

	String q3 = "select 'sakai_site', case when type is not null then type else 'null' end as type, 'placeholder', count (title) as count, 0 from sakai_site group by type";

	String q4 = "select 'sakai_realm', substr(provider_id, 0, 6), 'placeholder', count(*),0 from sakai_realm where provider_id like '____,_,%' group by substr(provider_id, 0, 6)";

	String q5 = "select 'sakai_site_tool', registration, 'placeholder', count(*), 0 from sakai_site_tool group by registration";

	String q6 = "select 'affiliate_usage', case when a.user_affiliation is null then 'unk' else a.user_affiliation end, case when e.event is null then 'null' else e.event end, count(*), 0 from sakai_user_id_map i left outer join db_monitor.user_affiliation a on i.user_id = a.user_id, sakai_session s, sakai_event e where s.session_user = i.user_id and s.session_id = e.session_id and e.event_date > (sysdate + interval '03:55' hour to minute) and e.event_date < (sysdate + interval '4' hour(1)) group by a.user_affiliation, e.event";

	String q7 = "select 'affiliate_usage', 'friend', e.event, count(*) from sakai_user_id_map i right outer join sakai_session s  on s.session_user = i.user_id, sakai_event e where e.event_date > (sysdate + interval '03:55' hour to minute) and e.event_date < (sysdate + interval '4' hour(1)) and i.eid like '%@%' group by a.user_affiliation, e.event";

	String q = q1
		+ " UNION " + q2
		+ " UNION " + q3
		+ " UNION " + q4
		+ " UNION " + q5
		+ " UNION " + q6
	;

	try {
	        Statement stmt = conn.createStatement();
	        ResultSet rset = stmt.executeQuery(q);
		ResultSetMetaData rsmd = rset.getMetaData();
	     	int numberOfColumns = rsmd.getColumnCount();
	        while (rset.next()){
			for ( int i = 1; i <= numberOfColumns; i++){ 
	                	String val = (rset.getString(i)).replaceAll("-.*", "");
%><%=val%>|<%
			}
%>
<%
	        }
		rset.close();
	        stmt.close();
	} catch (Exception ex){
		%>ERROR: Exception <%=ex%><%
		response.setStatus(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	}
%><%@ include file="common/dbfooter.jsp"
%><%@ include file="common/footer.jsp" %>
