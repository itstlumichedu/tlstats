#!/bin/bash
#
# Generate tlstats tarball with static files and compiled application
#
set -e
set -u

# Use this version value to uniquely identify the build contents.
# If not set in environment use default value so that it can run
# on a laptop for development.
: ${GIT_COMMIT:="LOCAL_BUILD"}

VERSION=${GIT_COMMIT}
ARTIFACTS_DIR=artifacts
TAR_NAME=tlstats.$VERSION.tar

######################

function writeEnvironmentVariables {
    vars=`cat <<EOF
# environment variables for installation of this TLStats build.
# $(date)
WEBRELSRC=http://limpkin.dsc.umich.edu:6660/job/
JOBNAME=${JOB_NAME:-LOCAL}
BUILD=${BUILD_NUMBER:-imaginary}
ARTIFACT_DIRECTORY=artifact
TIMESTAMP=${VERSION}
IMAGE_TYPE=tar
IMAGE_PREFIX=tlstats
CONFIG_PREFIX=
CONFIG_TYPE=
WEBAPPNAME=tlstats
#end
EOF`;
    echo "${vars}"
}

#######################

# Create versioned directory for complete set of deployable artifacts
[ -e $VERSION ] && rm -rf $VERSION
cp -Rp $ARTIFACTS_DIR $VERSION
mkdir $VERSION/webapps/

# Copy compiled datajsp.war file, prerequisite: cd dataapp; mvn install
cp dataapp/target/datajsp.war $VERSION/webapps

# Create tar file 
tar -cvf $TAR_NAME $VERSION
echo "packaged: $TAR_NAME at " $(date)

# Create installVariables.sh file and echo to console        
writeEnvironmentVariables >| installVariables.sh
writeEnvironmentVariables 

# cleanup
rm -Rf $VERSION

#end
